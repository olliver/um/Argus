from PyQt5 import QtCore, QtGui, QtQuick
from UM.Application import Application

##  Image provider for camera images recieved from scanner device
class CameraImageProvider(QtQuick.QQuickImageProvider):
    def __init__(self):
        QtQuick.QQuickImageProvider.__init__(self, QtQuick.QQuickImageProvider.Image)
        self._image = QtGui.QImage(15, 15, QtGui.QImage.Format_RGB888)
        self._backend = Application.getInstance().getBackend()
    
    def setImage(self,image):
        self._image = image
    
    def requestImage(self, id, size):
        return self._backend.getLatestCameraImage(), QtCore.QSize(15, 15)
