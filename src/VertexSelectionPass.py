# Copyright (c) 2016 Jaime van Kessel
# Argus is released under the terms of the AGPLv3 or higher.

import random

from UM.Resources import Resources
from UM.Application import Application

from UM.Math.Color import Color

from UM.Scene.Selection import Selection
from UM.Scene.ToolHandle import ToolHandle
from UM.Scene.Iterator.DepthFirstIterator import DepthFirstIterator

from UM.View.RenderPass import RenderPass
from UM.View.RenderBatch import RenderBatch
from UM.View.GL.OpenGL import OpenGL

##  A RenderPass subclass responsible for rendering vertices to a texture.
#
#   This pass performs the rendering of selectable vertices to a texture that can be
#   sampled to retrieve the actual vertex that was underneath the mouse cursor.
class VertexSelectionPass(RenderPass):
    def __init__(self, width, height):
        super().__init__("vertex_selection", width, height, -1000)

        self._shader = OpenGL.getInstance().createShaderProgram(Resources.getPath(Resources.Shaders, "default.shader"))
        self._gl = OpenGL.getInstance().getBindingsObject()
        self._scene = Application.getInstance().getController().getScene()

        self._renderer = Application.getInstance().getRenderer()


    def render(self):
        batch = RenderBatch(self._shader, mode = RenderBatch.RenderMode.Points)
        for node in DepthFirstIterator(self._scene.getRoot()):

            if node.isSelectable() and node.getMeshData():
                batch.addItem(transformation = node.getWorldTransformation(), mesh = node.getMeshData())
        self.bind()
        batch.render(self._scene.getActiveCamera())
        self.release()

    ##  Get the object id at a certain pixel coordinate.
    def getColorAtMousePosition(self, x, y):
        output = self.getOutput()

        window_size = self._renderer.getWindowSize()
        px = (0.5 + x / 2.0) * window_size[0]
        py = (0.5 + y / 2.0) * window_size[1]

        if px < 0 or px > (output.width() - 1) or py < 0 or py > (output.height() - 1):
            print(output.width(), px)
            return None

        return Color.fromARGB(output.pixel(px, py))

    '''def getOutput(self):
        import threading
        print("Getting OUTPUT;", self._fbo._contents, threading.currentThread())
        return self._fbo.getContents()'''

    def getColorAtPosition(self, x, y):
        output = self.getOutput()
        return Color.fromARGB(output.pixel(x, y))
