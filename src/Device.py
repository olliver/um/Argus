# Copyright (c) 2015 Ultimaker B.V.
# Argus is released under the terms of the AGPLv3 or higher.

##  Represents a scanner device.
#
#   This holds all information known about the device. This class is immutable.
class Device:
    ##  The IP address of the device.
    #
    #   It is stored as a single number. For localhost, the IP address 127.0.0.1
    #   is used, stored as 8323073 here.
    __ip_address = 0
    
    ##  The name of the device.
    #
    #   This name should be human-readable.
    __name = "Unknown device"
    
    ##  The number of cameras the device has.
    __num_cameras = 0
    
    ##  The number of projectors the device has.
    __num_projectors = 0
    
    ##  The number of platforms the device has.
    __num_platforms = 0
    
    ##  Creates a new device.
    #
    #   \param ip_address The IP address of the device.
    #   \param name The human-readable name of the device.
    #   \param num_cameras The number of cameras the device has.
    #   \param num_projectors The number of projectors the device has.
    #   \param num_platforms The number of platforms the device has.
    def __init__(self,ip_address,name,num_cameras,num_projectors,num_platforms):
        self.__ip_address = ip_address
        self.__name = name
        self.__num_cameras = num_cameras
        self.__num_projectors = num_projectors
        self.__num_platforms = num_platforms
    
    ##  Returns the IP address of the device.
    #
    #   \return The IP address of the device.
    def getIPAddress(self):
        return self.__ip_address
    
    ##  Returns the IP address of the device as a point-separated string.
    #
    #   The string is in the typical byte.byte.byte.byte notation. Only IPv4 is
    #   supported at the moment.
    #
    #   \return The IP address of the device, as string.
    def getIPAddressAsString(self):
        return "" + str(self.__ip_address >> 24 & 255) + "." + str(self.__ip_address >> 16 & 255) + "." + str(self.__ip_address >> 8 & 255) + "." + str(self.__ip_address & 255)
    
    ##  Returns the name of the device.
    #
    #   \return The name of the device.
    def getName(self):
        return self.__name
    
    ##  Returns the number of cameras of the device.
    #
    #   \return The number of cameras of the device.
    def getNumCameras(self):
        return self.__num_cameras
    
    ##  Returns the number of projectors of the device.
    #
    #   \return The number of projectors of the device.
    def getNumProjectors(self):
        return self.__num_projectors
    
    ##  Returns the number of platforms of the device.
    #
    #   \return The number of platforms of the device.
    def getNumPlatforms(self):
        return self.__num_platforms