from UM.Logger import Logger
from UM.Qt.QtApplication import QtApplication
from UM.Resources import Resources
from UM.Math.Vector import Vector
from UM.Scene.Camera import Camera
from UM.Operations.AddSceneNodeOperation import AddSceneNodeOperation
from UM.Math.Matrix import Matrix
from UM.Scene.Platform import Platform
from UM.Settings.ContainerRegistry import ContainerRegistry
from UM.Settings.ContainerStack import ContainerStack
from UM.Settings.InstanceContainer import InstanceContainer
from UM.Mesh.ReadMeshJob import ReadMeshJob


from UM.Preferences import Preferences

from PyQt5.QtCore import QUrl, pyqtSlot

from PyQt5.QtQml import qmlRegisterType, qmlRegisterSingletonType
from PyQt5.QtCore import Q_ENUMS

from . import CameraImageProvider
from . import ToolbarProxy
from . import ArgusSelectionPass

import os.path
import sys


def createToolbarProxy(engine, scriptEngine):
    return ToolbarProxy.ToolbarProxy()


class ScannerApplication(QtApplication):
    class ResourceTypes:
        QmlFiles = Resources.UserType + 1
        Firmware = Resources.UserType + 2
    Q_ENUMS(ResourceTypes)

    def __init__(self):
        Resources.addSearchPath(os.path.join(QtApplication.getInstallPrefix(), "share", "argus", "resources"))
        if not hasattr(sys, "frozen"):
            Resources.addSearchPath(os.path.join(os.path.abspath(os.path.dirname(__file__)), "..", "resources"))

        super().__init__(name = "argus", version = "14.2.1")
        Resources.addType(self.ResourceTypes.QmlFiles, "qml")
        Resources.addType(self.ResourceTypes.Firmware, "firmware")

        self._cloud_node_list = []
        self._colored_node_list = []
        self._platform = None
        self._camera_image_provider = None

    def initialize(self):
        super().initialize()
        ##  Initialise the version upgrade manager with Argus' storage paths.
        import UM.VersionUpgradeManager  # Needs to be here to prevent circular dependencies.
        UM.VersionUpgradeManager.VersionUpgradeManager.getInstance().setCurrentVersions(
            {
                ("preferences", Preferences.Version): (Resources.Preferences, "application/x-uranium-preferences")
            }
        )

        ContainerRegistry.getInstance().loadAllMetadata()

        self.setRequiredPlugins(["ScannerEngineBackend","PLYWriter","PLYReader"])
        self._camera_image_provider = CameraImageProvider.CameraImageProvider()
        self.engineCreatedSignal.connect(self._onEngineCreated)
        qmlRegisterSingletonType(ToolbarProxy.ToolbarProxy, "Argus", 1, 0, "ToolbarData", createToolbarProxy)
        self.getPreferences().setDefault("general/visible_settings", """
                calibration
                    setup_type
                    calibration_rotation_angle
                        projector_calibration_rotation_angle
                        platform_calibration_rotation_angle
                    number_of_calibration_boards
                        number_of_calibration_boards_scanner
                        number_of_calibration_boards_platform
                stitching
                    icp_correspondence_distance
                    global_correspondence_distance
                meshing
                    poisson_solver_depth
                scanning
                    pattern_type
                    phase_min_difference
                    number_of_scans_per_rotation
                    normal_estimation_radius
                    field_of_view
                    scan_min_distance
                    scan_max_distance
                    cutoff_below_platform
            """.replace("\n", ";").replace(" ", ""))

    @pyqtSlot(QUrl)
    def readLocalFile(self, file):
        if not file.isValid():
            return

        scene = self.getController().getScene()

        f = file.toLocalFile()
        extension = os.path.splitext(f)[1]
        filename = os.path.basename(f)

        job = ReadMeshJob(f)
        job.finished.connect(self._readMeshFinished)
        job.start()

    def _readMeshFinished(self, job):
        nodes = job.getResult()
        filename = job.getFileName()

        for node in nodes:
            node.setSelectable(True)
            node.setName(os.path.basename(filename))

            scene = self.getController().getScene()

            op = AddSceneNodeOperation(node, scene.getRoot())
            op.push()

            scene.sceneChanged.emit(node)

    def registerObjects(self, engine):
        engine.rootContext().setContextProperty("ArgusApplication", self)
        qmlRegisterSingletonType(
            QUrl.fromLocalFile(Resources.getPath(ScannerApplication.ResourceTypes.QmlFiles, "Actions.qml")), "Argus", 1, 0,
            "Actions")
        for path in Resources.getAllResourcesOfType(ScannerApplication.ResourceTypes.QmlFiles):
            type_name = os.path.splitext(os.path.basename(path))[0]
            if type_name in ("Argus", "Actions"):
                continue
            # Ensure that all qml files are registered in the Argus namespace
            qmlRegisterType(QUrl.fromLocalFile(path), "Argus", 1, 0, type_name)
    
    def addColorIndex(self, node):
        self._colored_node_list.append(node)
    
    def getColorIndex(self, node):
        return self._colored_node_list.index(node)
    
    def addCloudNode(self, cloud_node):
        self._cloud_node_list.append(cloud_node)
    
    def getCloudNodeList(self):
        return self._cloud_node_list
    
    def getCloudNodeIndex(self,cloud_node):
        return self._cloud_node_list.index(cloud_node)
    
    def getCloudNodeByIndex(self, index):
        return self._cloud_node_list[index]

    def _onEngineCreated(self):
        self._qml_engine.addImageProvider("camera", CameraImageProvider.CameraImageProvider())

        # DEBUG CODE
        '''group_node = SceneNode(name = "test")
        group_node.addDecorator(GroupDecorator())
        test_node = SceneNode(name = "test1")
        test_node2 = SceneNode(name = "test2")

        test_node.setMeshData(MeshData())
        test_node.setParent(group_node)
        test_node2.setParent(group_node)
        test_node2.setMeshData(MeshData())
        #print(" OMG")
        self.getController().getScene().getRoot().addChild(group_node)'''


        #group_node.addChild(SceneNode(name = "test1"))
        #group_node.addChild(SceneNode(name = "test2"))
        #group_node.addChild(SceneNode(name = "test3"))
        #group_node.addChild(SceneNode(name = "test4"))
        
    def _loadPlugins(self):
        self._plugin_registry.addPluginLocation(os.path.join(QtApplication.getInstallPrefix(), "lib", "scanner"))
        if not hasattr(sys, "frozen"):
            self._plugin_registry.addPluginLocation(os.path.join(os.path.abspath(os.path.dirname(__file__)), "..", "plugins"))
            self._plugin_registry.loadPlugin("ConsoleLogger")

        self._plugin_registry.loadPlugins()

        if self.getBackend() is None:
            raise RuntimeError("Could not load the backend plugin! This is required to function.")

    def _onRendererInitialized(self):
        self.getRenderer().removeRenderPass(self.getRenderer().getRenderPass("selection"))
        self.getRenderer().addRenderPass(ArgusSelectionPass.ArgusSelectionPass(self.getRenderer().getViewportWidth(), self.getRenderer().getViewportHeight()))

    def run(self):
        super().run()
        self.getController().setActiveView("SimpleView")
        self.getController().setCameraTool("CameraTool")
        #self.getController().setActiveTool("PointCloudAlignment")
        self.getController().setSelectionTool("SelectionTool")
        self.getRenderer().initialized.connect(self._onRendererInitialized)
        #self.getRenderer().removeRenderPass(self.getRenderer().getRenderPass("selection"))
        #self.getRenderer()._initialize() # Yeaah, shouldn't do that. I know.
        #self.getRenderer().addRenderPass(ArgusSelectionPass.ArgusSelectionPass(self.getRenderer().getViewportWidth(), self.getRenderer().getViewportHeight()))
        with self._container_registry.lockFile():
            self._container_registry.loadAllMetadata()

        root = self.getController().getScene().getRoot()
        self.setGlobalContainerStack(ContainerStack("global"))

        definition = ContainerRegistry.getInstance().findDefinitionContainers(name = "UltiScanner")

        if not definition:
            Logger.log("e", "Can't find any setting definition file with the name UltiScanner.")
            return
        definition = definition[0]
        self.getGlobalContainerStack().addContainer(definition)
        self._platform = Platform(root) #Get the platform that was in the definition.
        instances = ContainerRegistry.getInstance().findInstanceContainers(name = "ArgusScanner")
        if len(instances) == 0:
            instance = InstanceContainer("ArgusScanner")
        else:
            instance = instances[0] #Just take the first one.
        instance.setDefinition(definition.getId())
        self.getGlobalContainerStack().addContainer(instance)
        camera = Camera('3d', root)
        camera.translate(Vector(0, 150, -500))
        proj = Matrix()
        proj.setPerspective(45, 640/480, 1, 500)
        camera.setProjectionMatrix(proj)
        camera.setPerspective(True)
        camera.lookAt(Vector(0,0,0))
        
        self.getController().getScene().setActiveCamera('3d')
        camera_tool = self.getController().getTool("CameraTool")
       # if camera_tool:
            #camera_tool.setOrigin(Vector(400,-1000,5000)) #TODO hardcoded
            #camera_tool.setZoomRange(3500,10000)
        
        self.setMainQml(Resources.getPath(self.ResourceTypes.QmlFiles, "Scanner.qml"))
        self.initializeEngine()

        self._plugin_registry.checkRequiredPlugins(self.getRequiredPlugins())

        if self._qml_engine.rootObjects:
            self.exec_()
