from PyQt5.QtCore import QObject, pyqtSignal, pyqtSlot,pyqtSignal, pyqtProperty
from UM.Application import Application
from UM.Math.Vector import Vector

class ToolbarProxy(QObject):
    def __init__(self, parent = None):
        super().__init__(parent)
        self._state = "Setup"
        self._use_wizard = False
        self.setState("Setup")
    
    stateChanged = pyqtSignal()
    wizardStateChanged = pyqtSignal() # Wizard state is either simple or advanced
    
    @pyqtProperty(bool, notify = wizardStateChanged)
    def wizardActive(self):
        return self._use_wizard
    
    
    @pyqtSlot()
    def disableCameraTool(self):
        active_camera = Application.getInstance().getController().getScene().getActiveCamera()
        active_camera.setPosition(Vector(-115, 200, -800))
        active_camera.lookAt(Vector(-115,115,0))
        Application.getInstance().getController().setCameraTool("")
    
    @pyqtSlot()
    def enableCameraTool(self):
        active_camera = Application.getInstance().getController().getScene().getActiveCamera()
        active_camera.setPosition(Vector(0, 150, -500))
        active_camera.lookAt(Vector(0,0,0))
        Application.getInstance().getController().setCameraTool("CameraTool")
    
    @pyqtSlot(bool)
    def setWizardState(self, state):
        if state != self._use_wizard:
            self._use_wizard = state
            self.wizardStateChanged.emit()
    
    @pyqtProperty(str, notify = stateChanged)
    def state(self):
        return self._state
    
    @pyqtSlot(str)
    def setState(self, state):
        self._state = state
        backend = Application.getInstance().getBackend()
        if backend:
            backend.setProcessStep(state)
            
        self.stateChanged.emit()