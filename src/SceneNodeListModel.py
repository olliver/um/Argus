# Copyright (c) 2016 Jaime van Kessel & Ruben Dulek
# Argus is released under the terms of the AGPLv3 or higher.

from PyQt5.QtCore import Qt, QAbstractItemModel, QModelIndex, QVariant, pyqtSlot
from UM.Scene.Iterator.BreadthFirstIterator import BreadthFirstIterator
from UM.Application import Application

class SceneNodeListModel(QAbstractItemModel):
    NameRole = Qt.UserRole + 1

    def __init__(self, parent = None):
        super().__init__(parent)

        self._role_names = {
            self.NameRole: b"name"
        }

        self._root_node = Application.getInstance().getController().getScene().getRoot()
        self._root_node.childrenChanged.connect(self._onChildrenChanged)

    def _onChildrenChanged(self, node):
        # Reset the model if a node is changed. TODO; Do smarter stuff here.
        self.beginResetModel()
        self.endResetModel()

    def index(self, row, column, parent=QModelIndex()):
        if not self._root_node:
            return QModelIndex()

        if not self.hasIndex(row, column, parent):
            return QModelIndex()

        node = None
        if not parent.isValid():
            node = self._root_node.getChildren()[row]
        else:
            node = parent.internalPointer().getChildren()[row]

        return self.createIndex(row, column, node)

    def data(self, node, role):
        return QVariant(node.internalPointer().getName())


    def rowCount(self, parent = QModelIndex()):
        if not self._root_node:
            return 0
        if parent.column() > 0:
            return 0

        if not parent.isValid():
            return len(self._root_node.getChildren())

        node = parent.internalPointer()
        return len(node.getChildren())


    def columnCount(self, parent=QModelIndex()):
        return 1

    def parent(self, child):
        if not self._root_node:
            return QModelIndex()
        if not child.isValid():
            return QModelIndex()
        if not child.internalPointer():
            return QModelIndex()

        parent = child.internalPointer().getParent()
        if not parent:
            return QModelIndex()

        if not parent.getParent():  # The parent is root.
            row = 0
        else:  # Parent is not root; So we ask what the index of the parent is according to it's parent.
            row = parent.getParent().getChildren().index(parent)

        return self.createIndex(row, 0, parent)

    ##  Property for the rolenames
    def roleNames(self):
        return self._role_names

    @pyqtSlot(QModelIndex, result="QVariantMap")
    def get(self, index):
        definition = index.internalPointer()
