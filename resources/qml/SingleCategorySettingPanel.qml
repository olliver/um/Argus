// Copyright (c) 2015 Ultimaker B.V.
// Argus is released under the terms of the AGPLv3 or higher.
import QtQuick 2.2
import QtQuick.Controls 1.1
import QtQuick.Controls.Styles 1.1
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.1
import QtQuick.Window 2.1

import UM 1.2 as UM
import Argus 1.0 as Argus

Rectangle
{
    Layout.maximumWidth: parent.width
    Layout.minimumWidth: parent.width
    color: "#D8DEE6"
    height: 326
    id: background


    property variant setting_category;
    property variant base_item: base;

    property variant setting_width: UM.Theme.getSize("setting").width

    UM.I18nCatalog { id: catalog; name:"argus"}
    ScrollView
    {
        id: scrollview_base;

        style: UM.Theme.styles.scrollview

        property Action configureSettings;

        function showTooltip(item, position, text)
        {
            tooltip.text = text;
            position = item.mapToItem(base_item, position.x, position.y);
            tooltip.show(position);
        }

        function hideTooltip()
        {
            tooltip.hide();
        }

        anchors.topMargin: 5
        anchors.leftMargin: 5
        anchors.bottomMargin: 5
        anchors.rightMargin: 5
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom

        ListView
        {
            anchors.fill: parent
            model: UM.SettingDefinitionsModel
            {
                id: scanSettingsModel;
                containerId: "ultiscantastic";
                expanded: ["*"];
                rootKey: setting_category;
                visibilityHandler: UM.SettingPreferenceVisibilityHandler { }
            }

            delegate: Loader
            {
                id: settingLoader;
                width: parent.width
                height: provider.properties.enabled == "True" ? UM.Theme.getSize("section").height : 0
                opacity: provider.properties.enabled == "True" ? 1 : 0
                Behavior on opacity { NumberAnimation { duration: 100 } }
                Behavior on height { NumberAnimation { duration: 100 } }

                property var definition: model;
                property var settingDefinitionsModel: scanSettingsModel;
                property var propertyProvider: provider;


                //Qt5.4.2 and earlier has a bug where this causes a crash: https://bugreports.qt.io/browse/QTBUG-35989
                //In addition, while it works for 5.5 and higher, the ordering of the actual combo box drop down changes,
                //causing nasty issues when selecting different options. So disable asynchronous loading of enum type completely.
                asynchronous: model.type != "enum";
                active: model.type != undefined

                sourceComponent:
                {
                    switch(model.type)
                    {
                        case "int":
                            return settingTextField;
                        case "float":
                            return settingTextField;
                        case "enum":
                            return settingComboBox;
                        case "bool":
                            return settingCheckBox;
                        case "str":
                            return settingTextField;
                        case "category":
                            return settingCategory;
                        default:
                            return settingUnknown;
                    }
                }
                UM.SettingPropertyProvider
                {
                    id: provider

                    containerStackId: "global"
                    key: model.key
                    watchedProperties: [ "value", "enabled", "validationState", "state" ]
                    storeIndex: 0
                }
                Connections
                {
                    target: item
                    onShowTooltip: scrollview_base.showTooltip(settingLoader, { x: 0, y: settingLoader.height }, text)
                    onHideTooltip: scrollview_base.hideTooltip()
                }
            }

            add: Transition {
                SequentialAnimation {
                    NumberAnimation { properties: "height"; from: 0; duration: 100 }
                    NumberAnimation { properties: "opacity"; from: 0; duration: 100 }
                }
            }
            remove: Transition {
                SequentialAnimation {
                    NumberAnimation { properties: "opacity"; to: 0; duration: 100 }
                    NumberAnimation { properties: "height"; to: 0; duration: 100 }
                }
            }
            addDisplaced: Transition {
                NumberAnimation { properties: "x,y"; duration: 100 }
            }
            removeDisplaced: Transition {
                SequentialAnimation {
                    PauseAnimation { duration: 100; }
                    NumberAnimation { properties: "x,y"; duration: 100 }
                }
            }
        }
    }

    Component
    {
        id: settingTextField;

        Argus.SettingTextField { }
    }

    Component
    {
        id: settingComboBox;

        Argus.SettingComboBox { }
    }

    Component
    {
        id: settingCheckBox;

        Argus.SettingCheckBox { }
    }

    Component
    {
        id: settingCategory;

        Argus.SettingCategory { }
    }

    Component
    {
        id: settingUnknown;

        Argus.SettingUnknown { }
    }
}