import QtQuick 2.2
import QtQuick.Controls 1.1
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.1
import QtQuick.Window 2.1
import QtQuick.Controls.Styles 1.1

import Argus 1.0 as Argus
import UM 1.3 as UM

UM.MainWindow 
{
    id: main_window

    width: 1280
    height: 768
    viewportRect: Qt.rect(0, 0, (main_window.width - sidebar.width) / main_window.width, 1.0)
    title: qsTr("Argus");

    Item 
    {
        id: base;
        anchors.fill: parent;
        property int activeY

        UM.ApplicationMenu 
        {
            id: menu
            window: main_window

            Menu 
            {
                title: qsTr('&File');

                MenuItem { action: load_file_action; }
                RecentFilesMenu { }
                MenuItem { action: save_file_action; }
                MenuItem
                {
                    text: "Save Selection to File";
                    enabled: UM.Selection.hasSelection;
                    iconName: "document-save-as";
                    onTriggered: UM.OutputDeviceManager.requestWriteSelectionToDevice("local_file", "", {});
                }

                MenuSeparator { }

                MenuItem { action: quit_action; }
            }

            Menu 
            {
                title: qsTr('&Edit');

                MenuItem { action: undo_action; }
                MenuItem { action: redo_action; }
                MenuSeparator { }
                MenuItem { action: delete_action; }
            }

            Menu 
            {
                title: qsTr("&Machine");

                MenuSeparator { }
                MenuItem{text: "UltiScanTastic"; enabled: false;}
            }
            
            Menu 
            {
                id:extension_menu
                //: Extensions menu
                title: qsTr("E&xtensions");

                Instantiator 
                {
                    model: UM.extensionModel
                    id: blub
                    
                    Menu
                    {
                        title: model.name;
                        id: sub_menu
                        Instantiator
                        {
                            model: actions
                            MenuItem 
                            {
                                text:model.text
                                onTriggered: UM.extensionModel.subMenuTriggered(name,model.text)
                            }
                            onObjectAdded: sub_menu.insertItem(index, object)
                            onObjectRemoved: sub_menu.removeItem(object)
                            
                        }
       
                    }
                    onObjectAdded: extension_menu.insertItem(index, object)
                    onObjectRemoved: extension_menu.removeItem(object)
                }
            }

            Menu 
            {
                title: qsTr('&Settings');

                MenuItem { action: preferences_action; }
                MenuItem { action: plugin_action; }
                MenuItem { action: settings_action; }
            }

            Menu 
            {
                title: qsTr('&Help');

                MenuItem { action: helpAction; enabled: false; }
                MenuItem { action: aboutAction; enabled: false; }
            }
        }

        Item 
        {
            id: content_item;

            anchors.top: menu.bottom
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.right: parent.right

            Keys.forwardTo: menu

            DropArea 
            {
                anchors.fill: parent

                onDropped: 
                {
                    if(drop.urls.length > 0) 
                    {
                        for(var i in drop.urls) 
                        {
                            UM.MeshFileHandler.readLocalFile(drop.urls[i]);
                        }
                    }
                }
            }
            
            Toolbar 
            {
                id: toolbar;

                anchors 
                {
                    left: parent.left;
                    right: parent.right;
                    top: parent.top;
                }

            }
            Rectangle 
            {
                id: sidebar
                color: "white"
                anchors.right: parent.right
                anchors.top: toolbar.bottom
                anchors.bottom: parent.bottom
                width: 350
                property bool collapsed: false;
                
                DragableMeshListPanel
                {
                    id: mesh_list_panel;
                    anchors.margins: UM.Theme.getSize("default_margin").width

                    anchors.left: parent.left
                    anchors.right: parent.right
                    anchors.top: parent.top
                    anchors.bottom: bottomControlButtons.top
                }
                Item
                {
                    id: bottomControlButtons
                    width:parent.width
                    height: childrenRect.height
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: UM.Theme.getSize("default_margin").height;
                    WizardControlButton 
                    {
                        text: "Create Mesh"
                        anchors.left:parent.left
                        anchors.leftMargin: UM.Theme.getSize("default_margin").width;
                        width: 110
                        type: "light"
                        onClicked: Argus.ScannerEngineBackend.createMeshFromSelected()
                    }
                    WizardControlButton 
                    {
                        text: "Stitch"
                        anchors.right: parent.right
                        anchors.rightMargin: UM.Theme.getSize("default_margin").width
                        width: 110
                        onClicked: UM.Controller.setActiveTool("AutomaticPointcloudAlignment") 
                    }
                }
            }

            Column
            {   
                id: tool_area
                anchors.top: toolbar.bottom
                anchors.topMargin: 5
                anchors.leftMargin: 5
                anchors.left: parent.left
                visible: Argus.ToolbarData.state.indexOf("Edit") > -1;
                Repeater
                {
                    id: repeat

                    model: UM.ToolModel { }

                    Button
                    {
                        text: model.name
                        iconSource: UM.Theme.getIcon(model.icon);

                        checkable: true;
                        checked: model.active;
                        enabled: model.enabled

                        style: UM.Theme.styles.tool_button;
                        onCheckedChanged:
                        {
                            if(checked)
                            {
                                base.activeY = y
                            }
                        }
                        //Workaround since using ToolButton"s onClicked would break the binding of the checked property, instead
                        //just catch the click so we do not trigger that behaviour.
                        MouseArea {
                            anchors.fill: parent;
                            onClicked: {
                                parent.checked ? UM.Controller.setActiveTool(null) : UM.Controller.setActiveTool(model.id);
                            }
                        }
                    }
                }
            }
           
            Loader 
            {
                anchors.top: toolbar.bottom;
                anchors.right: parent.right;
                anchors.left: parent.left;
                anchors.bottom: parent.bottom;
                source: 
                {
                    if(Argus.ToolbarData.state.indexOf("Setup") > -1)
                    {
                        return "SetupScreen.qml";
                    }
                    if (Argus.ToolbarData.state.indexOf("Calibrate") > -1)
                    {
                        return "CalibrateScreen.qml";
                    }
                    if (Argus.ToolbarData.state.indexOf("Scan") > -1)
                    {
                        return "ScanScreen.qml";
                    }
                    if (Argus.ToolbarData.state.indexOf("Edit") > -1)
                    {
                        return "";
                    }
                }
                visible: Argus.ToolbarData.state != 3
                active: visible
            }

            Loader 
            {
                id: view_panel

                anchors.top: toolbar.bottom

                property var max_height: content_item.height - toolbar.height
                property var max_width: content_item.width - sidebar.width
                property var toolbar_height: toolbar.height

                height: childrenRect.height;
                z: -10000
                source: UM.ActiveView.valid ? UM.ActiveView.activeViewPanel : ""; 
            }
            
            ProgressBar 
            {
                id: progress_bar;

                minimumValue: 0;
                maximumValue: 100
                style: UM.Theme.styles.progressbar
                anchors.left: tool_area.right;
                anchors.top: toolbar.bottom
                anchors.topMargin: UM.Theme.getSize("default_margin").height;
                anchors.leftMargin: 440
                visible: false
                Connections 
                {
                    target: UM.Backend;
                    onProcessingProgress:
                    {
                        progress_bar.value = amount;
                        progress_bar.visible = (amount != 100 && amount != 0) ? true : false;
                    }
                }
            }

            Button 
            {
                visible: progress_bar.visible;
                text: "x"
                anchors.left: progress_bar.right
                anchors.leftMargin: 5
                anchors.verticalCenter: progress_bar.verticalCenter
                onClicked: 
                { 
                    Argus.ScannerEngineBackend.abortActiveProcess()
                    progress_bar.value = 100;
                }
                style: ButtonStyle { background:Item {} }
            }

            Item
            {
                anchors.left: tool_area.right;
                anchors.leftMargin: UM.Theme.getSize("default_margin").width;
                anchors.top: tool_area.top;
                anchors.topMargin: base.activeY
                width: childrenRect.width
                z:tool_area.z -1
                UM.PointingRectangle
                {
                    id: panelBorder;

                    z: tool_area.z -1

                    target: Qt.point(-UM.Theme.getSize("default_margin").width, UM.Theme.getSize("button").height/2)

                    arrowSize: UM.Theme.getSize("default_arrow").width

                    width: {
                        if (panel.item && panel.width > 0)
                        {
                             return Math.max(panel.width + 2 * UM.Theme.getSize("default_margin").width)
                        }
                        else
                        {
                            return 0
                        }
                    }
                    height: panel.item ? panel.height + 2 * UM.Theme.getSize("default_margin").height : 0;

                    opacity: panel.item && panel.width > 0 ? 1 : 0
                    Behavior on opacity { NumberAnimation { duration: 100 } }

                    color: UM.Theme.getColor("lining");

                    UM.PointingRectangle
                    {
                        id: panelBackground;

                        color: UM.Theme.getColor("tool_panel_background");
                        anchors.fill: parent
                        anchors.margins: UM.Theme.getSize("default_lining").width

                        target: Qt.point(-UM.Theme.getSize("default_margin").width, UM.Theme.getSize("button").height/2)
                        arrowSize: UM.Theme.getSize("default_arrow").width
                        MouseArea //Catch all mouse events (so scene doesnt handle them)
                        {
                            anchors.fill: parent
                        }
                    }

                    Loader
                    {
                        id: panel

                        x: UM.Theme.getSize("default_margin").width;
                        y: UM.Theme.getSize("default_margin").height;

                        source: UM.ActiveTool.valid ? UM.ActiveTool.activeToolPanel : "";
                        enabled: UM.Controller.toolsEnabled;
                    }
                }
            }
        }
        
        UM.MessageStack 
        {
            anchors.bottom: parent.bottom;
            anchors.horizontalCenter: parent.horizontalCenter;
            width: parent.width * 0.333;
            height: 250;
        }
        ToolTip
        {
            id:tooltip
        }
    }
    UM.PreferencesDialog
    {
        id: preferences

        Component.onCompleted:
        {
            //; Remove & re-add the general page as we want to use our own instead of uranium standard.
            removePage(1);
            insertPage(1, "Settings", Qt.resolvedUrl("SettingVisibilityPage.qml"));

            //Force refresh
            setPage(0);
        }

        onVisibleChanged:
        {
            // When the dialog closes, switch to the General page.
            // This prevents us from having a heavy page like Setting Visiblity active in the background.
            setPage(0);
        }
    }

    Action 
    {
        id: undo_action;
        text: "Undo";
        iconName: "edit-undo";
        shortcut: StandardKey.Undo;
        onTriggered: UM.OperationStack.undo();
        enabled: UM.OperationStack.canUndo;
    }

    Action 
    {
        id: redo_action;
        text: "Redo";
        iconName: "edit-redo";
        shortcut: StandardKey.Redo;
        onTriggered: UM.OperationStack.redo();
        enabled: UM.OperationStack.canRedo;
    }

    Action 
    {
        id: quit_action;
        text: "Quit";
        iconName: "application-exit";
        shortcut: StandardKey.Quit;
        onTriggered: Qt.quit();
    }

    Action 
    {
        id: preferences_action;
        text: "Preferences";
        iconName: "configure";
        onTriggered: preferences.visible = true;
    }
    
    Action
    {   
        id: plugin_action;
        text: "Plugins";
        onTriggered: 
        {
            preferences.visible = true;
            preferences.setPage(2);  
        }
    }

    Action
    {
        id: settings_action;
        text: "Settings";
        onTriggered:
        {
            preferences.visible = true;
            preferences.setPage(1);
        }
    }

    Action 
    {
        id: helpAction;
        text: "Show Manual";
        iconName: "help-contents";
        shortcut: StandardKey.Help;
    }

    Action 
    {
        id: aboutAction;
        text: "About...";
        iconName: "help-about";
    }

    Action 
    {
        id: delete_action;
        text: "Delete Selection";
        iconName: "edit-delete";
        shortcut: StandardKey.Delete;
        onTriggered: UM.Controller.removeSelection();
    }

    Action 
    {
        id: load_file_action;
        text: "Open...";
        iconName: "document-open";
        shortcut: StandardKey.Open;
        onTriggered: open_file_dialog.open()
    }

    Action 
    {
        id: save_file_action;
        text: "Save...";
        iconName: "document-save";
        shortcut: StandardKey.Save;
        onTriggered: UM.OutputDeviceManager.requestWriteToDevice("local_file", "", {})
    }

    Connections
    {
        target: Argus.Actions.configureSettingVisibility
        onTriggered:
        {
            preferences.visible = true;
            preferences.setPage(1);
            preferences.getCurrentItem().scrollToSection(source.key);
        }
    }

    Menu 
    {
        id: contextMenu;

        MenuItem { action: delete_action; }
    }
   
    FileDialog 
    {
        id: open_file_dialog

        title: "Choose files"
        modality: Qt.NonModal
        //TODO: Support multiple file selection, workaround bug in KDE file dialog
        //selectMultiple: true
        nameFilters: UM.MeshFileHandler.supportedReadFileTypes;
        onAccepted: 
        {
            UM.MeshFileHandler.readLocalFile(fileUrl)
        }
    }

    Timer
    {
        id: startupTimer;
        interval: 100;
        repeat: false;
        running: true;
        onTriggered:
        {
            if(!main_window.visible)
            {
                main_window.visible = true;
                restart();
            }
        }
    }
}
