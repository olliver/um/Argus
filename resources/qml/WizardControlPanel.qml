import QtQuick 2.2
import QtQuick.Controls 1.1
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.1
import QtQuick.Window 2.1
import UM 1.0 as UM
import QtQuick.Controls.Styles 1.1

Rectangle
{
    id: base
    property color border_color: "#898B91"
    border.width: 2
    border.color: border_color
    color: "#D8DEE6"
    width: 250
    height: 250
    property alias contents: contents_column.children
    property int content_margin: 10
    property alias left_button: left_button
    property alias right_button: right_button
    
    
    ColumnLayout
    {
        anchors.top:parent.top
        anchors.topMargin: content_margin
        anchors.left: parent.left
        anchors.leftMargin: content_margin
        width: base.width - content_margin * 2
        Layout.maximumHeight: base.height - content_margin * 2
        id: contents_column
    }
    
    ColumnLayout
    {
        id: buttons_item
        anchors.bottom: parent.bottom
        anchors.bottomMargin: content_margin
        anchors.left: parent.left
        anchors.leftMargin: content_margin
        anchors.right:parent.right
        anchors.rightMargin: content_margin
        
        WizardControlButton
        {
            id: left_button
            type: "light"
            text: qsTr("Previous step")
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            button_border_color: border_color
        }
        
        WizardControlButton 
        {
            id: right_button
            text: qsTr("Next step")
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            button_border_color:border_color
        }
    }
}