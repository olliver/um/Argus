import QtQuick 2.1
import QtQuick.Controls 1.1
import QtQuick.Controls.Styles 1.1
import QtQuick.Layouts 1.1

import "."
import UM 1.0 as UM
ButtonStyle 
{
    id: base;
    property bool down: control.pressed || (control.checkable && control.checked);

    property color highlight_color: UM.Theme.getColor("highlight");
    property color background_color: UM.Theme.getColor("toolbar_button_background");
    property color toggled_background_color: UM.Theme.getColor("toolbar_button_toggled_background");
    property color toggled_text_color: UM.Theme.getColor("primary_text")
    property color text_color:  UM.Theme.getColor("text_inactive")
    property color border_color: UM.Theme.getColor("toolbar_border")
    
    background: Item 
    {
        implicitWidth: control.width > 0 ? control.width : UM.Theme.getSize("toolbar_button").width;
        implicitHeight: control.height > 0 ? control.height : UM.Theme.getSize("toolbar_button").height;
        Rectangle 
        {
            id: background_item;

            anchors.fill: parent;
            color: base.background_color;
        }

        Rectangle
        {
            anchors.bottom: parent.bottom;
            anchors.left:parent.left;
            anchors.right:parent.right;
            height: 3
            color: down ? base.highlight_color: "transparent"
        }
        Rectangle
        {
            anchors.bottom: parent.bottom;
            anchors.left:parent.left;
            anchors.right:parent.right;
            height: 1
            color: base.border_color
        }
        
        states: 
        [
            State 
            {
                name: 'down';
                when: base.down;

                PropertyChanges { target: background_item; color: base.toggled_background_color; }
            }
        ]

        transitions:
        [
            Transition 
            {
                ColorAnimation { duration: 100; }
            }
        ]
    }
    
    label: Item
    {
        id:item
        property bool down: control.pressed || (control.checkable && control.checked);
        Column
        {  
            anchors.horizontalCenter:parent.horizontalCenter
            anchors.fill: parent;
            Label
            {
                id: text;
                anchors 
                {
                    horizontalCenter: parent.horizontalCenter
                    verticalCenter: parent.verticalCenter
                }
                text: control.text;
                color: base.text_color;
                height: 25
                width: 100
                font: UM.Theme.getFont("default_allcaps")
                horizontalAlignment: Text.AlignHCenter;
                wrapMode: Text.Wrap;
            }
        }  
            
        
        states: 
        [
            State 
            {
                name: 'down';
                when: base.down;

                //PropertyChanges { target: icon; color: base.foregroundHighlightColor; }
                PropertyChanges { target: text; color: base.toggled_text_color; }
            },
            State 
            {
                name: 'up';
                when: !base.down;

                //PropertyChanges { target: icon; color: base.foregroundDisabledColor; }
                PropertyChanges { target: text; color: base.text_color; }
            }
        ]

        transitions: 
        [
            Transition 
            {
                ColorAnimation { duration: 100; }
            }
        ]
    }
    
    
}
