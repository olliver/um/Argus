import QtQuick 2.2
import QtQuick.Controls 1.1
import QtQuick.Controls.Styles 1.1
import QtQuick.Layouts 1.1

import Argus 1.0 as Argus
import UM 1.0 as UM

import "."

Rectangle 
{
    id: base;

    color: UM.Theme.getColor("toolbar_background");
    height: UM.Theme.getSize("toolbar").height;

    property Action undo;
    property Action redo;
    property Action settings;

    Rectangle
    {
        anchors 
        {
            bottom: parent.bottom;
            left: parent.left;
            right: parent.right;
        }

        height: UM.Theme.getSize("toolbar_spacing").height;
        color: UM.Theme.getColor("toolbar_border");
    }

    Row
    {
        id: main_layout
        anchors.fill: parent;
        spacing: 0;

        Item
        {
            width: 100
            anchors.left: parent.left
            //anchors.verticalCenter:parent.verticalCenter
            anchors.top: parent.top
            height: parent.height
            RowLayout 
            {
                anchors.fill: parent;
                Image 
                { 
                    id: logo
                    anchors.centerIn: parent;
                    //source: UM.Resources.getIcon("scantastic_logo.png");
                }
                Rectangle 
                {
                    color: "black"
                    id: spacer1
                    width: 2
                    //height: parent.height - 15
                    anchors.left: logo.right
                    anchors.leftMargin :25
                    
                }
            }
        }
        
        Item
        {
            anchors.horizontalCenter: parent.horizontalCenter;
            anchors.top: parent.top
            width: 500
            ExclusiveGroup
            {
                id: toolbar_tab_group
                //Current is set here as the current can be changed either by the buttons themselves
                // or through other means, which caused inconsistencies with event updating
                // if only checked property was updated. 
                current: 
                {
                    if(Argus.ToolbarData.state.indexOf("Setup") > -1)
                    {
                        return setup_button
                    }
                    if(Argus.ToolbarData.state.indexOf("Calibrate") > -1)
                    {
                        return calibrate_button
                    }
                    if(Argus.ToolbarData.state.indexOf("Scan") > -1)
                    {
                        return scan_button
                    }
                    if(Argus.ToolbarData.state.indexOf("Edit") > -1)
                    {
                        return edit_button
                    }
                }
            }     
            Row
            {
                anchors.horizontalCenter: parent.horizontalCenter
                Layout.preferredWidth: 500
                spacing: 5
                Button
                {
                    id: setup_button
                    text: qsTr("Setup")
                    checkable: true
                    exclusiveGroup: toolbar_tab_group
                    style: ToolBarButtonStyleScan {}
                    width: UM.Theme.getSize("toolbar_button").width;
                    height: UM.Theme.getSize("toolbar_button").height;
                    onClicked:
                    {
                        Argus.ToolbarData.setState("Setup-1")
                    }
                }
                Button 
                {
                    id: calibrate_button
                    text: qsTr("Calibrate")
                    checkable: true
                    exclusiveGroup: toolbar_tab_group
                    style: ToolBarButtonStyleScan {}
                    width: UM.Theme.getSize("toolbar_button").width;
                    height: UM.Theme.getSize("toolbar_button").height;
                    onClicked: 
                    { 
                        Argus.ToolbarData.setState("Calibrate-1")
                    }
                }
                Button 
                {
                    id: scan_button
                    text: qsTr("Scan")
                    checkable: true
                    exclusiveGroup: toolbar_tab_group
                    style: ToolBarButtonStyleScan {}
                    width: UM.Theme.getSize("toolbar_button").width;
                    height: UM.Theme.getSize("toolbar_button").height;
                    onClicked: 
                    {
                        Argus.ToolbarData.setState("Scan-1")
                        Argus.ScannerEngineBackend.scan()
                    }
                }
                Button 
                {
                    id: edit_button
                    text: qsTr("Edit")
                    checkable: true
                    exclusiveGroup: toolbar_tab_group
                    style: ToolBarButtonStyleScan {}
                    width: UM.Theme.getSize("toolbar_button").width;
                    height: UM.Theme.getSize("toolbar_button").height;
                    onClicked: 
                    {
                        Argus.ToolbarData.setState("Edit-1")
                        Argus.ToolbarData.enableCameraTool()
                    }
                }
            }
        }
        Item
        {
            anchors.right: parent.right;
            anchors.top: parent.top
            width: 250
            height:parent.height
            Row
            {
                anchors.fill: parent;
                ToolButton
                {
                    id:settingsButton
                    anchors.right:parent.right
                    anchors.rightMargin:10
                    iconSource: UM.Theme.getIcon("save");
                    tooltip: "Settings and preferences"
                }
            }
        }      
    }
}