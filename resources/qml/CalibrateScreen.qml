import QtQuick 2.2
import QtQuick.Controls 1.1
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.1
import QtQuick.Window 2.1
import UM 1.0 as UM
import QtQuick.Controls.Styles 1.1

import Argus 1.0 as Argus
import UM 1.0 as UM

Rectangle //Background object
{
    id: background; 
    color: "#F9F9FB";
    anchors.fill: parent;
    property int content_width: 1120
    property int content_height: 650
    property int content_padding: 15
    property int state: //Read only
    {
        if (Argus.ToolbarData.state.indexOf("1") > -1)
        {
            return 1
        }
        if (Argus.ToolbarData.state.indexOf("2") > -1)
        {
            return 2
        }
        if (Argus.ToolbarData.state.indexOf("3") > -1)
        {
            return 3
        }
        if (Argus.ToolbarData.state.indexOf("4") > -1)
        {
            return 4
        }
        if(Argus.ToolbarData.state.indexOf("5") > -1)
        {
            return 5
        }
        return 1
    }
    Rectangle // White area behind content
    {
        id: content_background
        width: content_width + 3 * content_padding //3x padding; Left, center & right
        height: parent.height
        color: "white"
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        Item
        {
            id: content_item
            width: content_width
            height: content_height
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            Row
            {
                anchors.fill:parent
                Item
                {
                    id: content_left
                    width: parent.width / 2
                    height: parent.height
                    Image
                    {
                        id: camera_image
                        width: content_left.height
                        height: content_left.width 
                        transform:
                        [
                            Rotation { origin.x: 0; origin.y: 0; angle: -90},
                            Translate { y: content_height}
                        ]
                        source: Argus.ScannerEngineBackend.cameraImage
                    }
                }

                Item
                {
                    id: content_spacer
                    height: parent.height
                    width: 0.5 * content_padding
                }

                Item
                {
                    width: parent.width / 2
                    height: parent.height
                    id: content_right
                    Column
                    {
                        anchors.fill:parent
                        Row
                        {
                            id: calibration_phase_buttons
                            ExclusiveGroup
                            {
                                id: step_tab_group
                                current: 
                                switch(background.state)
                                {
                                    case 1:
                                        return focus_camera_button
                                    case 2:
                                        return focus_projector_button
                                    case 3:
                                        return exposure_camera_button
                                    case 4:
                                        return calibrate_button
                                    case 5:
                                        return platform_button
                                }
                            }

                            Button
                            {
                                text: "Camera"
                                id: focus_camera_button
                                exclusiveGroup: step_tab_group
                                checkable:true
                                onClicked: Argus.ToolbarData.setState("Calibrate-1")
                                width: 110
                                style: WizardTabButtonStyle {}
                            }

                            Button
                            {
                                text: "Projector"
                                id: focus_projector_button
                                exclusiveGroup: step_tab_group
                                checkable: true
                                onClicked: Argus.ToolbarData.setState("Calibrate-2")
                                width: 110
                                style: WizardTabButtonStyle {}
                            }

                            Button
                            {
                                text: "Exposure"
                                id: exposure_camera_button
                                exclusiveGroup: step_tab_group
                                checkable:true
                                onClicked: Argus.ToolbarData.setState("Calibrate-3")
                                width: 110
                                style: WizardTabButtonStyle {}
                            }
                            Button
                            {
                                text: "Calibrate"
                                id: calibrate_button
                                exclusiveGroup: step_tab_group
                                checkable:true
                                onClicked: Argus.ToolbarData.setState("Calibrate-4")
                                width: 110
                                style: WizardTabButtonStyle {}
                            }
                            Button
                            {
                                text: "Platform"
                                id: platform_button
                                exclusiveGroup: step_tab_group
                                checkable: true
                                onClicked: Argus.ToolbarData.setState("Calibrate-5")
                                width: 110
                                style: WizardTabButtonStyle {}
                            }
                        }

                        Text
                        {
                            id: instructions
                            text: switch(background.state)
                            {
                                case 1:
                                    return "Rotate the focus ring of the camera, until a sharp image can be seen. "
                                case 2:
                                    return "Rotate the focus slider of the projector, until a sharp image can be seen. "
                                case 3:
                                    return "Rotate the iris until barely any red pixels can be seen on the calibration board"
                                case 4:
                                    return "Calibration in progress."
                                case 5:
                                    return "Calibration of the platform in progress."
                            }
                            wrapMode: Text.Wrap
                            width: parent.width
                        }
                        Image
                        {
                            id: instruction_image
                            source: UM.Resources.getPath(UM.Resources.Images, "placeholder.png")
                        }

                        SingleCategorySettingPanel
                        {
                            setting_category: "calibration"
                            width: parent.width
                        }

                        Text
                        {
                            id: help_link
                            text: '<html><style type="text/css"></style><a href="scanner.ultimaker.com">Watch scanner calibration video online.</a></html>'
                            onLinkActivated: Qt.openUrlExternally(link)
                        }
                        WizardControlButton
                        {
                            id: left_button
                            type: "light"
                            text: qsTr("< Previous")
                            button_border_color: '#898B91'
                            visible: ! (Argus.ToolbarData.state.indexOf("1") > -1)
                            onClicked: switch(background.state)
                            {
                                case 2:
                                    Argus.ToolbarData.setState("Calibrate-1")
                                    break;
                                case 3:
                                    Argus.ToolbarData.setState("Calibrate-2")
                                    break;
                                case 4:
                                    Argus.ToolbarData.setState("Calibrate-3")
                                    break;
                                case 5:
                                    Argus.ToolbarData.setState("Calibrate-4")
                                    break;
                            }
                        }
                        WizardControlButton
                        {
                            id: next_button
                            type: "dark"
                            text: qsTr("Next >")
                            button_border_color: '#898B91'
                            visible: ! (Argus.ToolbarData.state.indexOf("5") > -1)
                            onClicked: switch(background.state)
                            {
                                case 1:
                                    Argus.ToolbarData.setState("Calibrate-2")
                                    break;
                                case 2:
                                    Argus.ToolbarData.setState("Calibrate-3")
                                    break;
                                case 3:
                                    Argus.ToolbarData.setState("Calibrate-4")
                                    break;
                                case 4:
                                    Argus.ToolbarData.setState("Calibrate-5")
                                    break;
                            }
                        }
                    }
                }
            }
        }
    }
}
