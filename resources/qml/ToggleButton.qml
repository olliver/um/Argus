import QtQuick 2.1
import QtQuick.Controls 1.1
import QtQuick.Controls.Styles 1.1

import UM 1.0 as UM

Button 
{
    id:base
    property string checkedImage
    property string uncheckedImage
    checkable:true
    style:ButtonStyle
    {
        background: Item
        {
            width: control.width > 0 ? control.width : 16;
            height: control.height > 0 ? control.height : 16;
        }
        label: Image
        {
            id:displayImage
            source: control.checked ? checkedImage:uncheckedImage
        }
    }
}