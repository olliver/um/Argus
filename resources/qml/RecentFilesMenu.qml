// Copyright (c) 2016 Ultimaker B.V.
// Cura is released under the terms of the AGPLv3 or higher.

import QtQuick 2.2
import QtQuick.Controls 1.1

import UM 1.2 as UM
import Argus 1.0 as Argus

Menu
{
    id: menu
    title: "Open Recent"
    iconName: "document-open-recent";

    enabled: ArgusApplication.recentFiles.length > 0;

    Instantiator
    {
        model: ArgusApplication.recentFiles
        MenuItem
        {
            text:
            {
                var path = modelData.toString()
                return (index + 1) + ". " + path.slice(path.lastIndexOf("/") + 1);
            }
            onTriggered: {
                ArgusApplication.readLocalFile(modelData);
                var meshName = backgroundItem.getMeshName(modelData.toString())
                backgroundItem.hasMesh(decodeURIComponent(meshName))
            }
        }
        onObjectAdded: menu.insertItem(index, object)
        onObjectRemoved: menu.removeItem(object)
    }
}
