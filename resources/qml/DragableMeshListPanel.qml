import QtQuick 2.1
import QtQuick.Controls 1.1
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.1
import QtQuick.Controls.Styles 1.1
import QtQml.Models 2.1

import UM 1.2 as UM

Rectangle 
{
    id: mesh_list_panel
    implicitWidth: 250
    implicitHeight: 500
    border.width: 1
    border.color:  UM.Theme.getColor("toolbar_border")

    ListView
    {
        id: root
        height: parent.height
        anchors.top: parent.top
        anchors.topMargin: parent.border.width
        anchors.right: parent.right
        anchors.rightMargin: 2 * parent.border.width
        anchors.left: parent.left
        anchors.leftMargin: 0
        displaced: Transition
        {
            NumberAnimation { properties: "x,y"; easing.type: Easing.OutQuad }
        }

        model: DelegateModel
        {
            id: visual_model
            model: UM.MeshListModel{}

            delegate: MouseArea
            {
                id: delegate_root
                property var key: model.key
                property int visual_index: DelegateModel.itemsIndex

                width: root.width
                opacity: model.is_group? 1: model.collapsed ? 0:1
                height:
                {
                    if(model.is_group)
                    {
                        return 25
                    }
                    if(!model.collapsed)
                    {
                        if(model.is_dummy)
                        {
                            return 10
                        }
                        return 25
                    }
                    return 0
                }
                enabled: !model.collapsed
                x:1
                onReleased: icon.Drag.drop()
                drag.target: icon
                drag.filterChildren: true

                Rectangle
                {
                    id: icon
                    anchors.fill: delegate_root
                    Rectangle
                    {
                        height: model.is_dummy ? 1 : 0
                        width: parent.width
                        color: "black"
                    }

                    opacity: parent.opacity
                    Behavior on opacity { NumberAnimation {} }
                    Behavior on height { NumberAnimation{} }
                    color:
                    {
                        if(model.is_group)
                        {
                            return "#FFFFFF";
                        }
                        if(model.is_dummy)
                        {
                            return "#FFFFFF";

                        } else
                        {
                            return "#EBEBEB"
                        }
                    }

                    Drag.active:{
                        if(!model.is_group && !model.is_dummy)
                        {
                            return delegate_root.drag.active
                        } else {
                            return false;
                        }
                    }
                    Drag.source: delegate_root
                    Drag.hotSpot.x: 0.5 * width
                    Drag.hotSpot.y: 0.5 * height
                    Menu
                    {
                        id: context_menu;
                        MenuItem
                        {
                            text: "Delete";
                            onTriggered: visual_model.model.removeMesh(model.key);
                        }
                        MenuItem
                        {
                            text: "Save";
                            onTriggered:
                            {
                                file_dialog.key = model.key;
                                file_dialog.open();
                            }
                        }
                    }
                    anchors
                    {
                        horizontalCenter: parent.horizontalCenter;
                        verticalCenter: parent.verticalCenter
                    }

                    Item
                    {
                        anchors.fill:parent

                        ToggleButton
                        {
                            id: visibility_button
                            onClicked: visual_model.model.setVisibility(model.key,checked)
                            checkedImage:  UM.Theme.getIcon("visible")
                            uncheckedImage: UM.Theme.getIcon("invisible")
                            checked: model.visibility
                            visible: !model.is_dummy
                            enabled: !model.is_dummy
                            width: 22
                            height: 22
                        }

                        ToggleButton
                        {
                            id:collapse_button
                            checkedImage : UM.Theme.getIcon("expanded")
                            uncheckedImage : UM.Theme.getIcon("collapsed")
                            visible: model.is_group ? true:false
                            checked: model.collapsed
                            onClicked: visual_model.model.setCollapsed(model.key)
                            anchors.left: visibility_button.right
                            width:  22
                            height: width
                        }
                        TextField
                        {
                            property bool editing_name: false
                            text:model.name
                            anchors.left: collapse_button.right
                            anchors.right: select_icon.left

                            onEditingFinished:
                            {
                                editing_name = false;
                                visual_model.model.setName(model.key,text)
                            }
                            id: name_text_field
                            readOnly: !name_text_field.editing_name && activeFocus
                            horizontalAlignment:TextInput.AlignLeft
                            visible: ! model.is_dummy
                            Component.onCompleted:
                            {
                                name_text_field.cursorPosition = 0
                            } //Hack to ensure that beginning of name is always visible

                            style: TextFieldStyle
                            {
                                background: Rectangle
                                {
                                    color: name_text_field.editing_name ?  "white":"transparent"
                                    radius: 2
                                    border.color: name_text_field.editing_name ?"black": "transparent"
                                    anchors.fill: parent
                                }
                            }
                            MouseArea
                            {
                                anchors.fill: parent;
                                anchors.horizontalCenter: parent.horizontalCenter;
                                anchors.verticalCenter: parent.verticalCenter
                                onClicked: {name_text_field.editing_name = true}
                                enabled: !name_text_field.editing_name
                            }
                        }

                        ToggleButton
                        {
                            id: select_icon
                            checkedImage: UM.Theme.getIcon("selected")
                            uncheckedImage: UM.Theme.getIcon("unselected")
                            anchors.right: parent.right
                            checked: model.selected
                            onClicked: visual_model.model.setSelected(model.key)
                            enabled: !model.is_dummy
                            visible: !model.is_dummy
                        }
                    }
                    radius: 3

                    states:
                    [
                        State
                        {
                            when: icon.Drag.active
                            ParentChange {
                                target: icon
                                parent: root
                            }

                            AnchorChanges
                            {
                                target: icon;
                                anchors.left:parent.left
                                anchors.horizontalCenter: undefined;
                                anchors.verticalCenter: undefined
                            }
                        },
                        State
                        {
                            when: !icon.Drag.active
                            AnchorChanges
                            {
                                target: icon;
                                anchors.left:parent.left
                            }
                        }
                    ]
                }

                DropArea
                {
                    anchors
                    {
                        fill: parent;
                    }

                    onEntered:
                    {
                        visual_model.items.move(drag.source.visual_index, delegate_root.visual_index)
                    }

                    onDropped:
                    {
                        visual_model.model.moveItem(drag.source.key, drag.source.visual_index)
                    }
                }

                MouseArea
                {
                    anchors.fill: parent;
                    acceptedButtons: Qt.RightButton;
                    onClicked: context_menu.popup();
                }
            }
        }
    }

    FileDialog 
    {
        id: file_dialog
        property variant key;

        title: "Save"
        modality: Qt.NonModal
        selectMultiple: false
        selectExisting:false

        onAccepted: 
        {
            meshList.model.saveMesh(key,fileUrl);
        }
    }
}