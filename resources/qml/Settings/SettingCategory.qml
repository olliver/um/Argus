// Copyright (c) 2015 Ultimaker B.V.
// Uranium is released under the terms of the AGPLv3 or higher.

import QtQuick 2.2
import QtQuick.Controls 1.1
import QtQuick.Controls.Styles 1.1
import QtQuick.Layouts 1.1

import UM 1.1 as UM
import Argus 1.0 as Argus

Rectangle {
    id: base;

    anchors.fill: parent;
    anchors.left: parent.left;
    anchors.leftMargin: UM.Theme.getSize("default_margin").width;
    anchors.right: parent.right;
    anchors.rightMargin: UM.Theme.getSize("default_margin").width;
    implicitHeight: UM.Theme.getSize("section").height;
    color: UM.Theme.getColor("setting_category_active");

    Text {
        text: definition.label

        anchors.fill: parent;
        font.family: UM.Theme.getFont("default");
        horizontalAlignment: Text.AlignHCenter;
        verticalAlignment: Text.AlignVCenter;
        color: UM.Theme.getColor("button_text");
    }

    UM.SimpleButton
    {
        id: settingsButton

        height: base.height * 0.6
        width: base.height * 0.6

        anchors {
            right:  parent.right
            rightMargin: UM.Theme.getSize("setting_preferences_button_margin").width / 2
            verticalCenter: parent.verticalCenter;
        }

        color: UM.Theme.getColor("setting_control_button");
        hoverColor: UM.Theme.getColor("setting_control_button_hover")
        iconSource: UM.Theme.getIcon("settings");

        onClicked: {
            Argus.Actions.configureSettingVisibility.trigger(definition)
        }
    }
}
