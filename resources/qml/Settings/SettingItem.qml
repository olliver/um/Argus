// Copyright (c) 2015 Ultimaker B.V.
// Uranium is released under the terms of the AGPLv3 or higher.

import QtQuick 2.1
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.1
import QtQuick.Controls.Styles 1.1

import UM 1.1 as UM

import "."

import Argus 1.0 as Argus

Item
{
    id: base

    height: UM.Theme.getSize("section").height

    property alias contents: controlContainer.children
    property alias hovered: mouse.containsMouse

    property var doDepthIndentation: true
    property var doQualityUserSettingEmphasis: true

    // Create properties to put property provider stuff in (bindings break in qt 5.5.1 otherwise)
    property var state: propertyProvider.properties.state
    property var stackLevels: propertyProvider.stackLevels
    property var stackLevel: stackLevels[0]

    signal contextMenuRequested()
    signal showTooltip(string text);
    signal hideTooltip();

    property string tooltipText:
    {
        var affects = settingDefinitionsModel.getRequiredBy(definition.key, "value")
        var affected_by = settingDefinitionsModel.getRequires(definition.key, "value")

        var affected_by_list = ""
        for(var i in affected_by)
        {
            affected_by_list += "<li>%1</li>\n".arg(affected_by[i].label)
        }

        var affects_list = ""
        for(var i in affects)
        {
            affects_list += "<li>%1</li>\n".arg(affects[i].label)
        }

        var tooltip = "<b>%1</b>\n<p>%2</p>".arg(definition.label).arg(definition.description)

        if(affects_list != "")
        {
            tooltip += "<br/><b>%1</b>\n<ul>\n%2</ul>".arg(catalog.i18nc("@label Header for list of settings.", "Affects")).arg(affects_list)
        }

        if(affected_by_list != "")
        {
            tooltip += "<br/><b>%1</b>\n<ul>\n%2</ul>".arg(catalog.i18nc("@label Header for list of settings.", "Affected By")).arg(affected_by_list)
        }

        return tooltip
    }

    MouseArea
    {
        id: mouse;

        anchors.fill: parent;

        acceptedButtons: Qt.RightButton;
        hoverEnabled: true;

        onClicked: base.contextMenuRequested();

        onEntered: {
            hoverTimer.start();
        }

        onExited: {
            if(controlContainer.item && controlContainer.item.hovered) {
                return;
            }
            hoverTimer.stop();
            base.hideTooltip();
        }

        Timer {
            id: hoverTimer;
            interval: 500;
            repeat: false;

            onTriggered:
            {
                base.showTooltip(base.tooltipText);
            }
        }

        Label
        {
            id: label;

            anchors.left: parent.left;
            anchors.leftMargin: doDepthIndentation ? definition.depth  * UM.Theme.getSize("setting_control_depth_margin").width : 0

            anchors.verticalCenter: parent.verticalCenter

            height: UM.Theme.getSize("section").height;
            verticalAlignment: Text.AlignVCenter;

            text: definition.label
            elide: Text.ElideMiddle;

            color: UM.Theme.getColor("setting_control_text");
            // emphasize the setting if it has a value in the user or quality profile
            font: base.doQualityUserSettingEmphasis && base.stackLevel != undefined && base.stackLevel <= 1 ? UM.Theme.getFont("default_italic") : UM.Theme.getFont("default")
        }

        Item
        {
            id: controlContainer;

            enabled: propertyProvider.isValueUsed

            anchors.right: parent.right;
            anchors.rightMargin: UM.Theme.getSize("default_margin").width
            anchors.verticalCenter: parent.verticalCenter;
            width: UM.Theme.getSize("setting_control").width;
            height: UM.Theme.getSize("setting_control").height
        }
    }

    UM.I18nCatalog { id: catalog; name: "argus" }
}
