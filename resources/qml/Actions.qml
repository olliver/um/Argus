// Copyright (c) 2016 Jaime van Kessel & Ruben Dulek
// Argus is released under the terms of the AGPLv3 or higher.

pragma Singleton

import QtQuick 2.2
import QtQuick.Controls 1.1
import UM 1.1 as UM
import Argus 1.0 as Argus

Item
{
    property alias configureSettingVisibility: configureSettingVisibilityAction

    Action
    {
        id: configureSettingVisibilityAction
        text: catalog.i18nc("@action:menu", "Configure setting visibility...");
        iconName: "configure"
    }
}
