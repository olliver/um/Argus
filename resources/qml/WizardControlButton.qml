import QtQuick 2.2
import QtQuick.Controls 1.1
import QtQuick.Layouts 1.1

import QtQuick.Controls.Styles 1.1
import UM 1.0 as UM


Button
{
    property int button_height: 25
    property int button_width: 100
    property color button_border_color: "#CCCCCC"
    property color dark_button_color: UM.Theme.getColor("highlight")
    property color light_button_color: "white"
    property color dark_text_color: "white"
    property color light_text_color: "#192742"
    property string type: "dark"
    
    id: base
    width: base.button_width
    height: base.button_height
    text: ""
    
    style: ButtonStyle 
    {
        background: Item 
        {
            implicitWidth: button_width;
            implicitHeight: button_height;
            Rectangle 
            {
                id: background;
                anchors.fill: parent;
                border.width: 1;
                border.color: type == "dark" ? "transparent" : button_border_color;
                color: type == "dark" ? dark_button_color : light_button_color
            }
        }
        label: Text 
        {
            text: base.text
            color: type == "dark" ? dark_text_color : light_text_color
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }
    }
}