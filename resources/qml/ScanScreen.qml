import QtQuick 2.2
import QtQuick.Controls 1.1
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.1
import QtQuick.Window 2.1
import QtQuick.Controls.Styles 1.1

import Argus 1.0 as Argus
import UM 1.0 as UM

Rectangle //Background object
{
    id: base;
    color: "transparent";
    anchors.fill: parent;
    property int content_width: 1120
    property int content_height: 650
    property int content_padding: 15
    property int state: //Read only
    {
        if (Argus.ToolbarData.state.indexOf("1") > -1)
        {
            return 1
        }
        if (Argus.ToolbarData.state.indexOf("2") > -1)
        {
            return 2
        }
        return 1
    }
    Rectangle
    {
        id: background_spacer_left
        color:"#F9F9FB"
        anchors.left:parent.left
        anchors.bottom:parent.bottom
        anchors.top:parent.top
        anchors.right: content_background.left
    }
    Rectangle
    {
        id: background_spacer_right
        color:"#F9F9FB"
        anchors.right:parent.right
        anchors.bottom:parent.bottom
        anchors.top:parent.top
        anchors.left: content_background.right
    }



    Rectangle // White area behind content
    {
        id: content_background
        width: content_width + 3 * content_padding //3x padding; Left, center & right
        height: parent.height
        color: "transparent"
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter

        Rectangle
        {
            color:"white"
            id: content_spacer_top
            anchors.top: parent.top
            anchors.left: content_background.left
            anchors.right: content_background.right
            anchors.bottom: content_item.top  
        }
        Rectangle
        {
            color:"white"
            id: content_spacer_bottom
            anchors.top: content_item.bottom
            anchors.left: content_background.left
            anchors.right: content_background.right
            anchors.bottom: parent.bottom
        }
        Rectangle
        {
            color:"white"
            id: content_spacer_left
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: content_item.left
            anchors.bottom: parent.bottom
        }
        Rectangle
        {
            color:"white"
            id: content_spacer_right
            anchors.top: parent.top
            anchors.left: content_item.right
            anchors.right: parent.right
            anchors.bottom: parent.bottom
        }

        Item
        {
            id: content_item
            width: content_width
            height: content_height
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            Row
            {
                anchors.fill:parent
                Item
                {
                    id: content_left
                    width: parent.width / 2 - 0.25 * content_padding
                    height: parent.height
                    Image
                    {
                        id: camera_image
                        width: content_left.height
                        height: content_left.width
                        visible: base.state == 1
                        transform:
                        [
                            Rotation { origin.x: 0; origin.y: 0; angle: -90},
                            Translate { y: content_height}
                        ]
                        source: Argus.ScannerEngineBackend.cameraImage
                    }
                }

                Rectangle
                {
                    id: content_spacer
                    height: parent.height
                    width: 0.5 * content_padding
                    color: "white"
                }

                Rectangle
                {
                    width: parent.width / 2 - 0.25 * content_padding
                    height: parent.height
                    id: content_right
                    color: "white"
                    Row
                    {
                        id: step_tab_row
                        anchors.top: parent.top
                        anchors.left: parent.left
                        anchors.right: parent.right
                        ExclusiveGroup
                        {
                            id: step_tab_group
                            current:
                            switch(base.state)
                            {
                                case 1:
                                    return setting_button
                                case 2:
                                    return scan_button
                            }
                        }

                        Button
                        {
                            text: "Scan Settings"
                            id: setting_button
                            exclusiveGroup: step_tab_group
                            checkable:true
                            onClicked: Argus.ToolbarData.setState("Scan-1")
                            width: 110
                            style: WizardTabButtonStyle {}
                        }

                        Button
                        {
                            text: "Scan"
                            id: scan_button
                            exclusiveGroup: step_tab_group
                            checkable: true
                            onClicked:
                            {
                                Argus.ToolbarData.setState("Scan-2")
                                Argus.ToolbarData.disableCameraTool()
                            }
                            width: 110
                            style: WizardTabButtonStyle {}
                        }
                    }

                    Text
                    {
                        id: instruction_text
                        text: switch(base.state)
                        {
                            case 1:
                                return "Nunc ac ante bibendum, feugiat metus quis, feugiat orci. Nunc effictur, velit id eleifend posuere nunc nunc condimentum tellus.";
                            case 2:
                                return "Scanning in progress. Capturing tachyon particles and re-arranging the universe.";
                        }
                        wrapMode: Text.Wrap
                        anchors.top: step_tab_row.bottom
                        anchors.left: parent.left
                        anchors.right: parent.right
                    }
                    Image
                    {
                        id: instruction_image
                        source: UM.Resources.getPath(UM.Resources.Images, "placeholder.png")
                        anchors.top: instruction_text.bottom
                    }
                    SingleCategorySettingPanel
                    {
                        id: scan_settings
                        setting_category: "scanning"

                        visible: base.state == 1
                        anchors.top: instruction_image.bottom
                        anchors.bottom: help_link.top
                        anchors.left: parent.left
                        anchors.right: parent.right
                    }
                    Text
                    {
                        id: help_link
                        text: '<html><style type="text/css"></style><a href="scanner.ultimaker.com">Help with setting up scanner profile.</a></html>'
                        onLinkActivated: Qt.openUrlExternally(link)
                        anchors.bottom: left_button.top
                        anchors.bottomMargin: 10
                    }

                    WizardControlButton
                    {
                        id: left_button
                        type: "light"
                        text: qsTr("< Calibrate")
                        anchors.left: parent.left
                        anchors.bottom: parent.bottom
                        button_border_color: '#898B91'
                        visible: (Argus.ToolbarData.state.indexOf("1") > -1)
                        onClicked: Argus.ToolbarData.setState("Calibrate-1")
                    }
                    WizardControlButton
                    {
                        id: next_button
                        type: "dark"
                        text: qsTr("Scan")
                        anchors.bottom: parent.bottom
                        button_border_color: '#898B91'
                        anchors.right: parent.right
                        visible: ! (Argus.ToolbarData.state.indexOf("2") > -1)
                        onClicked:
                        {
                            Argus.ToolbarData.setState("Scan-2")
                            Argus.ToolbarData.disableCameraTool()
                        }
                    }
                }
            }
        }
    }
    ToolTip
    {
        id: tooltip;
    }
}
