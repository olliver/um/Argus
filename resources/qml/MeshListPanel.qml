import QtQuick 2.1
import QtQuick.Controls 1.1
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.1
import QtQuick.Controls.Styles 1.1

import UM 1.0 as UM

Rectangle 
{
    id:meshListPanel
    width:250
    height:500
    border.width:1
    border.color:"black"
    Rectangle
    {
        id: dragHandle
        width: parent.width
        height: 16
        color: "white"
        radius: 0
        border.width: 1
        border.color: "#000000"
        anchors.top: parent.bottom
        Image
        {
            source : UM.Theme.icons.expand_small
            anchors.horizontalCenter:parent.horizontalCenter
        }
        MouseArea 
        {
            id: mouseAreaDragHandle
    
            property int oldMouseY
    
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            width: parent.width
            height: parent.height
            hoverEnabled: true
    
            onPressed: 
            {
                oldMouseY = mouseY
            }
    
            onPositionChanged: 
            {
                if (pressed) 
                {
                    meshListPanel.height = meshListPanel.height + (mouseY - oldMouseY)
                    meshListPanel.height = meshListPanel.height > 250 ? (meshListPanel.height < 625 ? meshListPanel.height: 625) : 250
                }
            }
        }
    }
    
    Item
    {
        id:mainContent
        anchors.fill:parent
        ColumnLayout 
        {
            anchors.fill:parent
            Layout.preferredHeight:400
            ListView
            {
                //width:parent.width - meshListPanel.border.width * 4
                anchors.left:parent.left
                anchors.leftMargin:1
                anchors.top:parent.top
                anchors.topMargin:1
                Layout.fillHeight: true
                Layout.fillWidth: true
                id:meshList
                delegate: meshDelegate      
                model: UM.meshListModel
            }
            
            Item
            {
                Layout.fillWidth: true
                height: 50
                Rectangle 
                {
                    color:"black"
                    width:parent.width
                    height:2
                }
                Rectangle
                {
                    anchors.fill:parent
                    border.width:1
                    RowLayout
                    {
                        width:parent.width - meshListPanel.border.width * 4
                        height:parent.height - meshListPanel.border.width * 2
                        anchors.horizontalCenter:parent.horizontalCenter
                        Label 
                        {
                            text:"0 layers of scans"
                        }
                        Button
                        {
                            iconSource: UM.Theme.icons.open
                            style:ButtonStyle{ background:Item{}}
                        }
                        
                        Button
                        {
                            iconSource: UM.Theme.icons.search
                            style:ButtonStyle{background:Item{}}
                        }
                        
                        Button
                        {
                            iconSource: UM.Theme.icons.trashbin
                            style:ButtonStyle{background:Item{}}
                            onClicked:
                            {
                                meshList.model.removeSelected()
                            }
                        }
                    }
                }
            }
        }
    }
    Component
    {
        id: meshDelegate
        //width:meshList.width -2
        //height:model.has_children ? 25: model.collapsed? 0:25
        //Behavior on height {NumberAnimation{}}
        MouseArea 
        {
            width:meshList.width -2
            height:model.is_group ? 25: model.collapsed? 0:25
            Behavior on height {NumberAnimation{}}
            onClicked: {console.log("YAYAAA")}
            id: delegateRoot
            
            Rectangle 
            {
                id:background
                color: model.depth == 1 ?"#FFFFFF": "#EBEBEB" 
                width:meshList.width -2
                Behavior on opacity { NumberAnimation { } }
                opacity: model.is_group? 1: model.collapsed ? 0:1
                
                Behavior on height {NumberAnimation{}}
                height:model.is_group ? 25: model.collapsed? 0:25
               
                
                Menu
                {
                    id: contextMenu;

                    MenuItem { text: "Delete"; onTriggered: meshList.model.removeMesh(model.key); }
                    MenuItem { text: "Save"; onTriggered: {fileDialog.key = model.key;
                                                            fileDialog.open();} }
                }
                
                RowLayout 
                {
                    anchors.fill:parent
                    
                    ToggleButton
                    { 
                        id: visibilityButton
                        onClicked: meshList.model.setVisibility(model.key,checked)
                        checkedImage:  UM.Theme.icons.visible
                        uncheckedImage: UM.Theme.icons.invisible
                        checked: model.visibility
                        width: 22
                    }
    
                    ToggleButton
                    {
                        id:collapseButton
                        checkedImage : UM.Theme.icons.expanded
                        uncheckedImage : UM.Theme.icons.collapsed
                        visible: model.depth != 1 ? false: model.is_group ? true:false
                        checked: model.collapsed
                        onClicked: meshList.model.setCollapsed(model.key)
                    }
                    TextField
                    {
                        property bool editingName: false
                        text:model.name
                        Layout.maximumWidth: 160
                        Layout.minimumWidth: 160
                        //anchors.horizontalCenter:parent.horizontalCenter
                        onEditingFinished:{editingName = false; meshList.model.setName(model.key,text)}
                        id: nameTextField
                        readOnly:!nameTextField.editingName && activeFocus
                        horizontalAlignment:TextInput.AlignLeft
                        Component.onCompleted:{ nameTextField.cursorPosition = 0 } //Hack to ensure that beginning of name is always visible
                        style: TextFieldStyle
                        {
                            background: Rectangle 
                            {
                                color: nameTextField.editingName ?  "white":"transparent"
                                radius:2
                                border.color:nameTextField.editingName ?"black": "transparent"
                                anchors.fill:parent
                            }
                        }
                        MouseArea
                        {
                            anchors.fill: parent;
                            anchors.horizontalCenter: parent.horizontalCenter; 
                            anchors.verticalCenter: parent.verticalCenter
                            onClicked: { nameTextField.editingName = true; }
                            enabled:!nameTextField.editingName
                        }
                    }

                    ToggleButton
                    {
                        id: lockButton
                        checkedImage: UM.Theme.icons.unlocked
                        uncheckedImage: UM.Theme.icons.locked  
                        anchors.right:selectIcon.left
                    } 
                    ToggleButton
                    {
                        id:selectIcon
                        checkedImage: UM.Theme.icons.selected
                        uncheckedImage: UM.Theme.icons.unselected
                        anchors.right:parent.right
                        checked: model.selected
                        onClicked: meshList.model.setSelected(model.key)
                    }
                }
                MouseArea
                {
                    anchors.fill: parent;
                    acceptedButtons: Qt.RightButton;
                    onClicked: contextMenu.popup();
                }
            }
        }
    }
    
    FileDialog 
    {
        id: fileDialog
        property variant key;

        title: "Save"
        modality: Qt.NonModal
        selectMultiple: false
        selectExisting:false

        onAccepted: 
        {
            meshList.model.saveMesh(key,fileUrl);
        }
    }
}
