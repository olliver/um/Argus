import QtQuick 2.2
import QtQuick.Controls 1.1
import QtQuick.Controls.Styles 1.1
import QtQuick.Layouts 1.1

import Argus 1.0 as Argus
import UM 1.0 as UM

Rectangle //Background panel.
{
    id: background
    color: "white"
    anchors.fill: parent
    property int content_padding: 15
    property int hardware_padding: 10
    property int state: //Read only.
    {
        if (Argus.ToolbarData.state.indexOf("1") > -1)
        {
            return 1
        }
        return 1
    }
    
    Rectangle //Main layout box.
    {
        id: content
        width: parent.width - content_padding * 2
        height: parent.height - content_padding * 2
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        
        Column //Left column with instructions.
        {
            id: instructions_column
            width: (parent.width - content_padding) * 3 / 4
            height: parent.height
            anchors.left: parent.left
            anchors.top: parent.top
            Text
            {
                id: instructions_text
                width: parent.width
                wrapMode: Text.WordWrap
                text: "<h1>Setting up your scanner</h1>To set up your scanner, please attach the Argus scanning cabinet and the Argus rotating platform to the spacing arm as shown in this animation."
            }
            Item
            {
                id: instructions_image
                width: 800
                height: 500
                Image
                {
                    id: arm_image
                    source: UM.Resources.getPath(UM.Resources.Images, "arm.svg")
                    anchors.left: instructions_image.left
                    anchors.top: instructions_image.top
                    sourceSize.height: parent.height
                    sourceSize.width: height * sourceSize.width / sourceSize.height
                }
                Image
                {
                    source: UM.Resources.getPath(UM.Resources.Images, "cabinet.svg")
                    anchors.left: instructions_image.left
                    SequentialAnimation on anchors.leftMargin
                    {
                        NumberAnimation
                        {
                            from: 0
                            to: -20 * instructions_image.width / 400
                            duration: 700
                            easing.type: Easing.OutExpo
                        }
                        NumberAnimation
                        {
                            from: -20 * instructions_image.width / 400
                            to: -28 * instructions_image.width / 400
                            duration: 300
                            easing.type: Easing.OutExpo
                        }
                        PauseAnimation { duration: 500 }
                        NumberAnimation
                        {
                            from: -28 * instructions_image.width / 400
                            to: 0
                            duration: 250
                            easing.type: Easing.OutExpo
                        }
                        loops: Animation.Infinite
                    }
                    anchors.top: instructions_image.top
                    SequentialAnimation on anchors.topMargin
                    {
                        NumberAnimation
                        {
                            from: 0
                            to: 77 * instructions_image.height / 300
                            duration: 700
                            easing.type: Easing.OutExpo
                        }
                        NumberAnimation
                        {
                            from: 77 * instructions_image.height / 300
                            to: 80 * instructions_image.height / 300
                            duration: 300
                            easing.type: Easing.OutExpo
                        }
                        PauseAnimation { duration: 500 }
                        NumberAnimation
                        {
                            from: 80 * instructions_image.height / 300
                            to: 0
                            duration: 250
                            easing.type: Easing.OutExpo
                        }
                        loops: Animation.Infinite
                    }
                    sourceSize.height: 200 * parent.height / 300
                    sourceSize.width: height * sourceSize.width / sourceSize.height
                }
                Image
                {
                    source: UM.Resources.getPath(UM.Resources.Images, "platform.svg")
                    anchors.left: instructions_image.left
                    anchors.leftMargin: 215 * parent.width / 400
                    anchors.top: instructions_image.top
                    SequentialAnimation on anchors.topMargin
                    {
                        NumberAnimation
                        {
                            from: 150 * instructions_image.height / 300
                            to: 255 * instructions_image.height / 300
                            duration: 1000
                            easing.type: Easing.OutExpo
                        }
                        PauseAnimation { duration: 500 }
                        NumberAnimation
                        {
                            from: 255 * instructions_image.height / 300
                            to: 150 * instructions_image.height / 300
                            duration: 250
                            easing.type: Easing.OutExpo
                        }
                        loops: Animation.Infinite
                    }

                    sourceSize.height: 25 * parent.height / 300
                    sourceSize.width: height * sourceSize.width / sourceSize.height
                }
            }
        }
        
        Rectangle //Padding between the two columns.
        {
            id: content_spacer
            width: content_padding
            height: parent.height
            anchors.left: instructions_column.right
            anchors.top: parent.top
        }
        
        Rectangle //Right column with devices.
        {
            id: devices_column
            width: (parent.width - content_padding) / 4
            height: parent.height
            anchors.left: content_spacer.right
            anchors.top: parent.top
            Rectangle //Title above the device list.
            {
                id: devices_title
                width: parent.width
                height: 40
                anchors.top: parent.top
                anchors.left: parent.left
                Text
                {
                    width: parent.width
                    height: parent.height
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    text: "<h1>Devices</h1>"
                }
            }
            Rectangle //Divider line.
            {
                id: devices_divider
                width: parent.width
                height: 1
                anchors.top: devices_title.bottom
                anchors.topMargin: 2
                color: "black"
            }
            ListView //Actual list of devices.
            {
                id: devices_list
                width: parent.width
                anchors.top: devices_divider.bottom
                anchors.topMargin: 2
                anchors.bottom: parent.bottom
                model: UM.ScannerEngineBackend.devicesModel //Gets the list of devices.
                delegate: Button //The item's clickable area.
                {
                    id: device_delegate
                    width: devices_list.width
                    height: 20
                    tooltip: model.name + " at " + model.ipAddress + " with " + model.numCameras + " cameras, " + model.numProjectors + " projectors and " + model.numPlatforms + " platforms.";
                    exclusiveGroup: select_device_group
                    style: ButtonStyle //Remove the standard OS-themed button style. Just make it a plain background.
                    {
                        background: Rectangle
                        {
                            color: UM.ScannerEngineBackend.devicesModel.currentDevice == model.ipAddress ? (control.hovered ? UM.Theme.getColor("button_active_hover") : UM.Theme.getColor("button_active")) : (control.hovered ? UM.Theme.getColor("button") : "white");
                            //color: "transparent"
                        }
                    }
                    onClicked:
                    {
                        UM.ScannerEngineBackend.devicesModel.selectDevice(model.ipAddress)
                    }
                    
                    Label //The device name at the left.
                    {
                        id: device_name
                        width: parent.width
                        anchors.left: parent.left
                        anchors.leftMargin: hardware_padding
                        verticalAlignment: Text.AlignVCenter
                        text: model.name
                    }
                    Label //The number of platforms of the device.
                    {
                        id: device_platforms_count
                        anchors.right: parent.right
                        anchors.rightMargin: hardware_padding //Always include padding at the right, even if there is no platform.
                        anchors.top: parent.top
                        anchors.topMargin: 2
                        verticalAlignment: Text.AlignVCenter
                        text: model.numPlatforms > 0 ? model.numPlatforms : ""
                    }
                    Image //The icon for the platform count.
                    {
                        id: device_platforms_icon
                        height: 16
                        anchors.right: device_platforms_count.left
                        anchors.top: parent.top
                        anchors.topMargin: 2
                        source: model.numPlatforms > 0 ? UM.Theme.getIcon("platform") : ""
                        fillMode: Image.PreserveAspectFit
                    }
                    Label //The number of projectors of the device.
                    {
                        id: device_projectors_count
                        anchors.right: device_platforms_icon.left
                        anchors.rightMargin: model.numProjectors > 0 ? hardware_padding : 0
                        anchors.top: parent.top
                        anchors.topMargin: 2
                        verticalAlignment: Text.AlignVCenter
                        text: model.numProjectors > 0 ? model.numProjectors : ""
                    }
                    Image //The icon for the projector count.
                    {
                        id: device_projectors_icon
                        height: 16
                        anchors.right: device_projectors_count.left
                        anchors.top: parent.top
                        anchors.topMargin: 2
                        source: model.numProjectors > 0 ? UM.Theme.getIcon("projector") : ""
                        fillMode: Image.PreserveAspectFit
                    }
                    Label //The number of cameras of the device.
                    {
                        id: device_cameras_count
                        anchors.right: device_projectors_icon.left
                        anchors.rightMargin: model.numProjectors > 0 ? hardware_padding : 0
                        anchors.top: parent.top
                        anchors.topMargin: 2
                        verticalAlignment: Text.AlignVCenter
                        text: model.numCameras > 0 ? model.numCameras : ""
                    }
                    Image //The icon for the camera count.
                    {
                        id: device_cameras_icon
                        height: 16
                        anchors.right: device_cameras_count.left
                        anchors.top: parent.top
                        anchors.topMargin: 2
                        source: model.numCameras > 0 ? UM.Theme.getIcon("camera") : ""
                        fillMode: Image.PreserveAspectFit
                    }
                }
            }
        }
    }
    
    ExclusiveGroup //Button group for the device selection.
    {
        id: select_device_group
    }  
}