from UM.Event import MouseEvent
from UM.Tool import Tool
from UM.Math.Vector import Vector
from UM.Event import Event
from UM.Application import Application
from . import VertexSelectionToolHandle

from src.VertexSelectionPass import VertexSelectionPass

from PyQt5.QtGui import qAlpha, qRed, qGreen, qBlue

class VertexSelectionTool(Tool):
    def __init__(self):
        super().__init__()

        self._scene = Application.getInstance().getController().getScene()
        self._renderer = Application.getInstance().getRenderer()
        self._handle = VertexSelectionToolHandle.VertexSelectionToolHandle()
        self._selected_point_coordinates = None

    def event(self, event):
        if event.type == Event.ToolActivateEvent:
            self._renderer.addRenderPass(VertexSelectionPass(self._renderer.getViewportWidth(), self._renderer.getViewportHeight()))
        elif event.type == Event.ToolDeactivateEvent:
            self._renderer.removeRenderPass(self._renderer.getRenderPass("vertex_selection"))
        if event.type == MouseEvent.MouseReleaseEvent and MouseEvent.LeftButton in event.buttons:
            pixel_color = self._renderer.getRenderPass("vertex_selection").getColorAtMousePosition(event.x,event.y)
            if pixel_color:
                if pixel_color.a:
                    index  = int(255 - pixel_color.a*255)
                    targeted_node = Application.getInstance().getCloudNodeByIndex(index)
                    #Pixel colors are encoded in (r,g,b) with r being least significant and b being most.
                    pixel_index = int(pixel_color.r *255) + (int(pixel_color.g * 255) << 8) + (int(pixel_color.b * 255) << 16)
                    temp = targeted_node.getMeshDataTransformed().getVertex(pixel_index)
                    self._selected_point_coordinates = Vector(temp[0],temp[1],temp[2])

                    self._handle.setParent(self.getController().getScene().getRoot())
                    self._handle.setPosition(self._selected_point_coordinates)
