from . import VertexSelectionTool

from UM.i18n import i18nCatalog
i18n_catalog = i18nCatalog("plugins")

def getMetaData():
    return {
        "tool": {
            "name": i18n_catalog.i18nc("Vertex Select Tool name", "Vertex Selection"),
            "description": i18n_catalog.i18nc("Vertex Select Tool description", "Vertex slection"),
            "icon": "vertex_selection"
        },
        "cura": {
            "tool": {
                "visible": False
            }
        }
    }

def register(app):
    return {"tool": VertexSelectionTool.VertexSelectionTool()}
