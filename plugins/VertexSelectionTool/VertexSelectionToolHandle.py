from UM.Scene.ToolHandle import ToolHandle
from UM.Mesh.MeshData import MeshData
from UM.Mesh.MeshBuilder import MeshBuilder
from UM.Math.Vector import Vector

class VertexSelectionToolHandle(ToolHandle):
    def __init__(self, parent = None):
        super().__init__(parent)

        #self.setPointMesh(lines)
        mb = MeshBuilder()
        mb.addCube(
            width = 1,
            height = 1,
            depth = 1,
            center = Vector(0, 0, 0),
            color = ToolHandle.XAxisSelectionColor
        )

        self.setSolidMesh(mb.build())