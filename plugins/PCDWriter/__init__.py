from . import PCDWriter

def getMetaData():
    return {
        "mesh_writer": {
            "output": [
                {
                    "extension": "pcd",
                    "description": "PCD file"
                }
            ]
        }
    }

def register(app):
    return {}
    # The plugin is broken right now. Not that high priority to fix.
    return {"mesh_writer":PCDWriter.PCDWriter()}
