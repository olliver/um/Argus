from UM.Mesh.MeshReader import MeshReader
from UM.Mesh.MeshData import MeshData
from UM.Mesh.MeshData import MeshType
from UM.Logger import Logger
from UM.Scene.PointCloudNode import PointCloudNode

import struct
import numpy


##    In it's current implementation it reads *only* PointClouds (with or without normals).
#     It uses the definition as found on http://paulbourke.net/dataformats/ply/
class PLYReader(MeshReader):
    def __init__(self):
        super(PLYReader, self).__init__()
        self._supported_extensions = [".ply"]
        
    def read(self, file_name):
        x_location = -1
        y_location = -1
        z_location = -1
        nx_location = -1
        ny_location = -1
        nz_location = -1
        element_location = -1
        read_type = -1
        num_vertices = -1
        formatting = b''
        vertex_byte_size = 0
        try:
            f = open(file_name, "rb")
        except FileNotFoundError:
            Logger.log("w","File %s was not found",file_name)
            return None

        # Read header
        for index in range(0,250):  # It's fairly unlikely that the header will be over 250 lines.
            line = f.readline()
            line = line.decode("utf-8")  # We read it as bytes, but the header is made up from strings
            parts = line.split()
            if index == 0:
                if parts[0] != "ply" and parts[0] != "PLY":
                    Logger.log("e", "The ply file does not adhere to the ply standard. Aborting.")
                    return  # First line does not have the magic number. Stop right away!
            if parts[0] == "format":
                if parts[1] == "ascii":
                    read_type = 0
                if parts[1] == "binary_little_endian":
                    read_type = 1
                    formatting += b'<'

                if parts[1] == "binary_big_endian":
                    formatting += b'>'

            if parts[0] == "end_header":  # End of header, stop reading header (start reading vertex data)
                break
            if parts[0] == "element":
                if parts[1] == "vertex":  # Found vertex element
                    element_location = index
                    num_vertices = int(parts[2])

            if parts[0] == "property":  # Check ordering
                if parts[2] == "x":
                    x_location = index - element_location - 1
                if parts[2] == "y":
                    y_location = index - element_location - 1
                if parts[2] == "z":
                    z_location = index - element_location - 1
                if parts[2] == "nx":
                    nx_location = index - element_location - 1
                if parts[2] == "ny":
                    ny_location = index - element_location - 1
                if parts[2] == "nz":
                    nz_location = index - element_location - 1

                if parts[1] == 'float':  # check type!
                    formatting += b'f'
                    vertex_byte_size += 4

        has_normals = (nx_location != -1 and ny_location != -1 and nz_location != -1)
        vertices = numpy.zeros((num_vertices, 3), dtype = numpy.float32)
        normals = numpy.zeros((num_vertices, 3), dtype = numpy.float32)
        # Read vertex data
        if read_type == 0:  # ASCII
            for index, line in enumerate(f.readlines()):
                if index > num_vertices:  # The ply can have edges, but we ignore them as we interpret it as a point cloud
                    break
                line = line.decode("utf-8")  # This is ascii type, not bytes
                parts = line.split()
                vertices[index, 0] = parts[x_location]
                vertices[index, 1] = parts[y_location]
                vertices[index, 2] = parts[z_location]
                if has_normals:
                    normals[index, 0] = parts[nx_location]
                    normals[index, 1] = parts[ny_location]
                    normals[index, 2] = parts[nz_location]
        else:  # binary
            for index in range(0, num_vertices):
                data = struct.unpack(formatting, f.read(vertex_byte_size))
                vertices[index, 0] = data[x_location]
                vertices[index, 1] = data[y_location]
                vertices[index, 2] = data[z_location]
                if has_normals:
                    normals[index, 0] = data[nx_location]
                    normals[index, 1] = data[ny_location]
                    normals[index, 2] = data[nz_location]
        f.close()

        mesh = MeshData(vertices, normals, type = MeshType.pointcloud)
        Logger.log("d", "Loaded a mesh with %s vertices", mesh.getVertexCount())
        result = PointCloudNode()  # Wrap a node around it.
        result.setMeshData(mesh)
        result.setSelectable(True)
        return result
