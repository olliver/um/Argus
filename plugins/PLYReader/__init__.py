from . import PLYReader

def getMetaData():
    return {
        "mesh_reader": [{
            "extension": "ply",
            "description": "PLY File"
        }]
    }


def register(app):
    return {"mesh_reader":PLYReader.PLYReader()}
