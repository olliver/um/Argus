# Copyright (c) 2016 Jaime van Kessel & Ruben Dulek
# Cura is released under the terms of the AGPLv3 or higher.

import os.path

from UM.Application import Application
from UM.PluginRegistry import PluginRegistry

from UM.View.RenderPass import RenderPass
from UM.View.RenderBatch import RenderBatch
from UM.View.GL.OpenGL import OpenGL

from UM.Mesh.MeshBuilder import MeshBuilder  # To create the billboard quad
from PyQt5.QtGui import QImage
from PyQt5.QtCore import Qt

class VertexErasePass(RenderPass):
    def __init__(self, width, height):
        super().__init__("vertex_erase", width, height)

        self._shader = None
        self._gl = OpenGL.getInstance().getBindingsObject()
        self._scene = Application.getInstance().getController().getScene()
        self._billboard_quad = self._createBillboardQuad(2)
        self._texture = OpenGL.getInstance().createTexture()

        # Create a shader that renders the quad to full screen.
        self._shader = OpenGL.getInstance().createShaderProgram(
            os.path.join(PluginRegistry.getInstance().getPluginPath("VertexEraseTool"), "erase.shader"))
        self._scene = Application.getInstance().getController().getScene()

        self._image = QImage(Application.getInstance().getMainWindow().width(), Application.getInstance().getMainWindow().height(), QImage.Format_ARGB32)
        self._image.fill(Qt.white)

    def render(self):
        self.bind()
        batch = RenderBatch(self._shader, backface_cull = False)
        batch.addItem(self._scene.getRoot().getWorldTransformation(), self._billboard_quad)

        self._texture = OpenGL.getInstance().createTexture()
        self._texture.setImage(self._image)
        self._shader.setTexture(0, self._texture)

        batch.render(self._scene.getActiveCamera())

        self.release()

    def _createBillboardQuad(self, size):
        mb = MeshBuilder()
        mb.addFaceByPoints(-size / 2, -size / 2, 0, -size / 2, size / 2, 0, size / 2, -size / 2, 0)
        mb.addFaceByPoints(size / 2, size / 2, 0, -size / 2, size / 2, 0, size / 2, -size / 2, 0)

        # Set UV coordinates so a texture can be created
        mb.setVertexUVCoordinates(0, 0, 1)
        mb.setVertexUVCoordinates(1, 0, 0)
        mb.setVertexUVCoordinates(4, 0, 0)
        mb.setVertexUVCoordinates(2, 1, 1)
        mb.setVertexUVCoordinates(5, 1, 1)
        mb.setVertexUVCoordinates(3, 1, 0)

        return mb.build()