# Copyright (c) 2016 Jaime van Kessel and Ruben Dulek
# Argus is released under the terms of the AGPLv3 or higher.

from UM.View.View import View
from UM.Scene.Iterator.DepthFirstIterator import DepthFirstIterator
from UM.Scene.PointCloudNode import PointCloudNode
from UM.Event import Event
from UM.PluginRegistry import PluginRegistry
import os.path
from UM.View.GL.OpenGL import OpenGL
from . import VertexErasePass


class VertexEraseView(View):
    def __init__(self):
        super().__init__()

        self._erase_composite_shader = None
        self._composite_pass = None
        self._old_composite_shader = None
        self._old_layer_bindings = None
        self._erase_pass = None

    def beginRendering(self):
        scene = self.getController().getScene()
        renderer = self.getRenderer()

        # Only render pointclouds.
        for node in DepthFirstIterator(scene.getRoot()):
            if isinstance(node, PointCloudNode):
                node.render(renderer)

    def endRendering(self):
        pass

    def event(self, event):
        if event.type == Event.ViewActivateEvent:
            if not self._erase_pass:
                # Currently the RenderPass constructor requires a size > 0
                # This should be fixed in RenderPass's constructor.
                self._erase_pass = VertexErasePass.VertexErasePass(1, 1)
                self.getRenderer().addRenderPass(self._erase_pass)
            if not self._erase_composite_shader:
                self._erase_composite_shader = OpenGL.getInstance().createShaderProgram(os.path.join(PluginRegistry.getInstance().getPluginPath("VertexEraseTool"), "erase_composite.shader"))

            if not self._composite_pass:
                self._composite_pass = self.getRenderer().getRenderPass("composite")

            self._old_layer_bindings = self._composite_pass.getLayerBindings()

            self._composite_pass.setLayerBindings(["default", "vertex_erase"])

            self._old_composite_shader = self._composite_pass.getCompositeShader()
            self._composite_pass.setCompositeShader(self._erase_composite_shader)

        elif event.type == Event.ViewDeactivateEvent:
            self._composite_pass.setLayerBindings(self._old_layer_bindings)
            self._composite_pass.setCompositeShader(self._old_composite_shader)