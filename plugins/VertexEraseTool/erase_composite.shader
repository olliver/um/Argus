[shaders]
vertex =
    uniform highp mat4 u_modelViewProjectionMatrix;
    attribute highp vec4 a_vertex;
    attribute highp vec2 a_uvs;

    varying highp vec2 v_uvs;

    void main()
    {
        gl_Position = u_modelViewProjectionMatrix * a_vertex;
        v_uvs = a_uvs;
    }

fragment =
    uniform sampler2D u_layer0;
    uniform sampler2D u_layer1;

    varying vec2 v_uvs;

    void main()
    {
        vec4 result = vec4(0.965, 0.965, 0.965, 1.0);
        vec4 layer0 = texture2D(u_layer0, v_uvs);

        if(texture2D(u_layer1, v_uvs).r > 0.5)
        {
            result = layer0 * layer0.a + result * (1.0 - layer0.a);
        }
        gl_FragColor = result;
    }

[defaults]
u_layer0 = 0
u_layer1 = 1

[bindings]

[attributes]
a_vertex = vertex
a_uvs = uv

