[shaders]
vertex =
    uniform highp mat4 u_modelMatrix;

    attribute highp vec4 a_vertex;
    attribute highp vec2 a_uvs;

    varying highp vec3 v_vertex;
    varying highp vec2 v_uvs;

    void main()
    {
        vec4 world_space_vert = u_modelMatrix * a_vertex;
        gl_Position = a_vertex;

        v_vertex = a_vertex.xyz;

        v_uvs = a_uvs;
    }

fragment =
    uniform sampler2D u_texture;

    varying highp vec3 v_vertex;
    varying highp vec2 v_uvs;

    void main()
    {
        gl_FragColor = texture2D(u_texture, v_uvs);
    }

[defaults]
u_texture = 0

[attributes]
a_vertex = vertex
a_uvs = uv0
