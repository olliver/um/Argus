from UM.Event import MouseEvent
from UM.Logger import Logger
from UM.Tool import Tool
from UM.Event import Event
from UM.Application import Application

from PyQt5.QtGui import QImage, QPainter, QPen, QBrush
from PyQt5.QtCore import Qt
from src.VertexSelectionPass import VertexSelectionPass
from .EraseVertexJob import EraseVertexJob
import threading
import time

##    The Vertex erase tool enables the user to erase vertices as if doing so on a 2D plane.
#     In order to make it run smooth, a trick is performed. Instead of constantly updating the mesh_data (which would
#     result in a lot of data copying) it first hides the vertices that needs to be removed, by drawing on a full screen
#     quad and using a special shader.
#
#     Once the mouse is released, a EraseVertexJob is started which iteratively uses that image, combined with a
#     rendered selection pass to actually do the deletions.
#
#     This requires the job to notify the main thread that a re-render is required, which it does by means of a signal.
class VertexEraseTool(Tool):
    def __init__(self):
        super().__init__()

        self._scene = Application.getInstance().getController().getScene()
        self._renderer = Application.getInstance().getRenderer()

        self._pressed = False
        self._cloud_index_dict = {}
        self._old_camera_tool = None
        self._previous_view = None

        self._texture_image = None
        self._painter = None

        self._last_x = None
        self._last_y = None

        self._tool_size = 20
        self._line_pen = QPen(Qt.black)
        self._line_pen.setWidth(self._tool_size)
        self._line_pen.setCapStyle(Qt.RoundCap)
        self._circle_pen = QPen(Qt.black)
        self._erase_vertex_job = None
        self._single_erase_vertex_job = None
        self._vertex_selection_image = None

        self._vertex_selection_image_lock = threading.Lock()

    ##  Ensure that a new vertex selection image is created.
    def updateVertexSelectionImage(self):
        self._vertex_selection_image_lock.acquire()
        self._vertex_selection_image = self._renderer.getRenderPass("vertex_selection").getOutput()
        self._vertex_selection_image_lock.release()

    def getVertexSelectionImage(self):
        self._vertex_selection_image_lock.acquire()
        if self._vertex_selection_image:
            result = self._vertex_selection_image.copy()
        else:
            result = None
        self._vertex_selection_image_lock.release()
        return result

    def _updateMainWindow(self):
        self._renderer.getRenderPass("vertex_selection").render()

    def onRenderCompleted(self):
        self.updateVertexSelectionImage()

    def _onEraseVertexCompleted(self, job):
        # Reset image
        self._texture_image = QImage(Application.getInstance().getMainWindow().width(),
                                     Application.getInstance().getMainWindow().height(), QImage.Format_ARGB32)
        self._texture_image.fill(Qt.white)
        self._renderer.getRenderPass("vertex_erase")._image = self._texture_image
        Application.getInstance().getMainWindow().update()  # Force screen update

        self._painter = None
        Application.getInstance().getController().setCameraTool(self._old_camera_tool)  # Re-enable camera

        self._erase_vertex_job.updateWindow.disconnect(self._updateMainWindow)
        self._erase_vertex_job.finished.disconnect(self._onEraseVertexCompleted)
        self._erase_vertex_job = None
        Application.getInstance().getMainWindow().setAllowResize(True)

    def _startFullEraseJob(self, job = None):
        Application.getInstance().getMainWindow().update()
        self._erase_vertex_job.run_once = False
        self._erase_vertex_job.finished.connect(self._onEraseVertexCompleted)

        self._erase_vertex_job._lock.acquire()
        Application.getInstance().getMainWindow().update()
        Application.getInstance().getMainWindow().setAllowResize(False)
        self._erase_vertex_job.start()

    def event(self, event):
        if event.type == Event.MousePressEvent and MouseEvent.LeftButton in event.buttons:
            if self._erase_vertex_job is not None and not self._erase_vertex_job.isFinished():
                return # Don't start just yet!
            self._pressed = True

            # Disable the camera controls
            self._old_camera_tool = Application.getInstance().getController().getCameraTool()
            Application.getInstance().getController().setCameraTool(None)

            # Create tools for painting
            self._texture_image = QImage(Application.getInstance().getMainWindow().width(), Application.getInstance().getMainWindow().height(), QImage.Format_ARGB32)
            self._texture_image.fill(Qt.white)
            self._painter = QPainter(self._texture_image)
            self._painter.setBrush(QBrush(Qt.black))
            self._last_x = Application.getInstance().getMainWindow().mouseX
            self._last_y = Application.getInstance().getMainWindow().mouseY
            self._painter.setPen(self._circle_pen)
            self._painter.drawEllipse(Application.getInstance().getMainWindow().mouseX - self._tool_size / 2 , Application.getInstance().getMainWindow().mouseY - self._tool_size / 2, self._tool_size, self._tool_size)
        elif event.type == Event.MouseReleaseEvent and MouseEvent.LeftButton in event.buttons:
            self._pressed = False

            # Actually erase the vertices that have only been hidden before.
            self._erase_vertex_job = EraseVertexJob(self._texture_image)
            self._erase_vertex_job.updateWindow.connect(self._updateMainWindow)
            if not self._single_erase_vertex_job.isFinished():
                self._single_erase_vertex_job.finished.connect(self._startFullEraseJob)
            # If the single job is still running, wait for that to complete. Start removing right away otherwise.
            else:
                self._startFullEraseJob()

        elif event.type == Event.MouseMoveEvent and self._pressed: # Mouse move while left mouse button is pressed.
            if self._painter:
                self._painter.setPen(self._line_pen)
                self._painter.drawLine(self._last_x, self._last_y, Application.getInstance().getMainWindow().mouseX, Application.getInstance().getMainWindow().mouseY)
                self._last_x = Application.getInstance().getMainWindow().mouseX
                self._last_y = Application.getInstance().getMainWindow().mouseY
                self._renderer.getRenderPass("vertex_erase")._image = self._texture_image
                if self._single_erase_vertex_job is None or self._single_erase_vertex_job.isFinished():
                    self._single_erase_vertex_job = EraseVertexJob(self._texture_image)
                    self._single_erase_vertex_job._lock.acquire()
                    Application.getInstance().getMainWindow().update()
                    self._single_erase_vertex_job.updateWindow.connect(self._updateMainWindow)
                    Application.getInstance().getMainWindow().setAllowResize(False)
                    self._single_erase_vertex_job.start()
                else:
                    pass
            else:
                Logger.log("e", "Multiple vertex erase jobs are interfering with each other!")

        # Check for tool activation & deactivation events
        elif event.type == Event.ToolActivateEvent:
            Application.getInstance().getMainWindow().renderCompleted.connect(self.onRenderCompleted)
            self._previous_view = Application.getInstance().getController().getActiveView().getPluginId()
            Application.getInstance().getController().setActiveView("VertexEraseTool")
            self._renderer.addRenderPass(VertexSelectionPass(self._renderer.getViewportWidth(), self._renderer.getViewportHeight()))
        elif event.type == Event.ToolDeactivateEvent:
            Application.getInstance().getMainWindow().renderCompleted.disconnect(self.onRenderCompleted)
            self._renderer.removeRenderPass(self._renderer.getRenderPass("vertex_selection"))
            Application.getInstance().getController().setActiveView(self._previous_view)