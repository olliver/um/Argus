from . import VertexEraseTool
from . import VertexEraseView
from UM.i18n import i18nCatalog
i18n_catalog = i18nCatalog("plugins")


def getMetaData():
    return {
        "tool": {
            "name": i18n_catalog.i18nc("Erase tool toolbar button", "Erase"),
            "description": i18n_catalog.i18nc("erase tool description", "Remove points"),
            "icon": "erase"
        },
        "view":
        {
            "name": "VertexEraseView",
            "visible": False
        }
    }


def register(app):
    return {"tool": VertexEraseTool.VertexEraseTool(), "view": VertexEraseView.VertexEraseView()}
