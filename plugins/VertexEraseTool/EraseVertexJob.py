# Copyright (c) 2016 Jaime van Kessel & Ruben Dulek
# Argus is released under the terms of the AGPLv3 or higher.
from UM.Job import Job
from UM.Application import Application
from UM.Logger import Logger
import numpy
import qimage2ndarray
import threading
from UM.Signal import Signal, signalemitter


@signalemitter
class EraseVertexJob(Job):
    def __init__(self, texture_image):
        super().__init__()
        self._renderer = Application.getInstance().getRenderer()
        self._scene = Application.getInstance().getController().getScene()
        self._texture_image = texture_image
        self._cloud_index_dict = {}
        self._vertex_selection = self._renderer.getRenderPass("vertex_selection")
        self._vertex_selection_image = None
        Application.getInstance().getMainWindow().renderCompleted.connect(self.onRenderCompleted)
        self._lock = threading.Lock()

        self.run_once = True

    updateWindow = Signal(type = Signal.Queued)

    def onRenderCompleted(self):
        # If the lock was set, a new render should release it.
        if self._lock.locked():
            self._lock.release()

    def run(self):
        Logger.log("d", "Erase job is staring to remove vertices.")
        vertices_removed = 0
        num_iterations = 0

        # Create a numpy view on the texture data
        numpy_texture = qimage2ndarray.recarray_view(self._texture_image)
        running = True
        while running:
            if self.run_once:
                running = False
            # Grab the lock so that in the next pass we only continue after an updated render.
            result = self._lock.acquire()
            Job.yieldThread()
            num_iterations += 1

            if num_iterations % 5 == 0 :
                print("Erase is at iteration", num_iterations)

            new_selection_image = Application.getInstance().getController().getActiveTool().getVertexSelectionImage()

            if self._vertex_selection_image == new_selection_image:
                # Image is exactly the same, even though we removed stuff. Doing the same thing again won't change
                # anything, so we can stop.
                break
            self._vertex_selection_image = new_selection_image

            # Counter used to track the amount of pixels we are actually removing.
            marked_pixels = 0
            self._cloud_index_dict = {}

            # Create a numpy view on the vertex selection qimage
            numpy_vertex_selection = qimage2ndarray.recarray_view(self._vertex_selection_image)

            # Find all coordinates where the numpy texture is white and the alpha canel of the vertex selection > 0
            y_coordinates, x_coordinates = numpy.bitwise_and(numpy_texture.g < 127,
                                                             numpy_vertex_selection.a > 0).nonzero()
            for y, x in zip(y_coordinates, x_coordinates):
                pixel_color = numpy_vertex_selection[y][x]
                marked_pixels += 1

                cloud_index = int(255 - pixel_color.a)

                # Pixel colors are encoded in (r,g,b) with r being least significant and b being most.
                pixel_index = int(pixel_color.r) + (int(pixel_color.g) << 8) + (int(pixel_color.b) << 16)
                if cloud_index not in self._cloud_index_dict:
                    self._cloud_index_dict[cloud_index] = [pixel_index]
                else:
                    self._cloud_index_dict[cloud_index].extend([pixel_index])

            self._scene.acquireLock()
            self._scene.setIgnoreSceneChanges(True)
            for entry in self._cloud_index_dict:
                # Because the mesh_data is immutable, we need to do some tricks to actually remove data.
                mesh_data = Application.getInstance().getCloudNodeByIndex(entry).getMeshData()
                vertices = mesh_data.getVertices()
                normals = mesh_data.getNormals()
                new_vertices = numpy.delete(vertices, self._cloud_index_dict[entry], 0)
                new_normals = numpy.delete(normals, self._cloud_index_dict[entry], 0)
                mesh_data = mesh_data.set(vertices=new_vertices, normals=new_normals)

                # Update the mesh data with the new mesh_data
                Application.getInstance().getCloudNodeByIndex(entry).setMeshData(mesh_data)

                # Ensure that we don't have issues with the axis aligned bounding box being calculated
                Application.getInstance().getCloudNodeByIndex(entry)._calculate_aabb = False
            self._scene.releaseLock()
            self._scene.setIgnoreSceneChanges(False)
            # Send a signal to indicate we want a render update (this job can't do it, as it's not on the main thread.
            self.updateWindow.emit()

            vertices_removed += marked_pixels
            # No more marked pixels, stop this job
            if not marked_pixels:
                break
        Logger.log("d", "Erase job completed removing %s vertices. With %s iterations", vertices_removed, num_iterations)