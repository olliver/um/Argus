# Copyright (c) 2015 Ultimaker B.V.
# Argus is released under the terms of the AGPLv3 or higher.
from UM.Qt.ListModel import ListModel
from UM.Application import Application

from PyQt5.QtCore import Qt,pyqtProperty,pyqtSlot,pyqtSignal

from src.Device import Device

##  Model to list the scanner devices currently detected.
#
#   This model is shown in the set-up panel. The user can then select one of
#   these devices to scan with.
class ScannerDevicesModel(ListModel):
    ##  Role representing the IP address of the device as a string.
    IPAddressRole = Qt.UserRole + 1
    
    ##  Role giving a human-readable name to the device.
    NameRole = Qt.UserRole + 2
    
    ##  Role to store the number of cameras the device has.
    NumCamerasRole = Qt.UserRole + 3
    
    ##  Role to store the number of projectors the device has.
    NumProjectorsRole = Qt.UserRole + 4
    
    ##  Role to store the number of platforms the device has.
    NumPlatformsRole = Qt.UserRole + 5
    
    ##  The currently selected device.
    #
    #   This holds the IP address of the currently selected device.
    _current_device = None
    
    ##  Signals that the currently selected device has been changed.
    #
    #   This then triggers the UI to update the background colour of the
    #   appropriate button.
    deviceChanged = pyqtSignal()

    ##  Creates the scanner devices model.
    #
    #   The model is assigned five roles: ipAddress, name, numCameras,
    #   numProjectors and numPlatforms. These roles can be accessed from in QML.
    #
    #   \param parent The parent model. Should be None.
    def __init__(self, parent = None):
        super().__init__(parent)
        self.addRoleName(self.IPAddressRole,"ipAddress")
        self.addRoleName(self.NameRole,"name")
        self.addRoleName(self.NumCamerasRole,"numCameras")
        self.addRoleName(self.NumProjectorsRole,"numProjectors")
        self.addRoleName(self.NumPlatformsRole,"numPlatforms")
        #self.addDevice(Device(2130706433,"Nonexistent test device",1,0,0))
    
    ##  Adds a new device to the model.
    #
    #   The device is converted into a dictionary for the list model to access
    #   the roles of.
    #
    #   \param device The device to add to this model.
    def addDevice(self,device):
        print("Adding device ",device.getName())
        self.appendItem({
            "ipAddress": device.getIPAddressAsString(), #One dictionary item for each role.
            "name": device.getName(),
            "numCameras": device.getNumCameras(),
            "numProjectors": device.getNumProjectors(),
            "numPlatforms": device.getNumPlatforms()
        })
        if(self._current_device == None):
            self.selectDevice(device.getIPAddressAsString()) #Select the first item by default.
        print("Device model now has " + str(self.rowCount()) + " devices.")
    
    ##  Changes the currently selected device.
    #
    #   \param ip_address The IP address of the new device to select. Must be a
    #   string.
    @pyqtSlot(str)
    def selectDevice(self,ip_address):
        if(self._current_device == ip_address): #If the device does not change, don't abort or send settings or anything.
            return
        Application.getInstance().getMachineManager().getActiveProfile().setSettingValue("host",ip_address) #Set the internal setting.
        Application.getInstance().getBackend().sendSetting("host",ip_address,"string") #Set the back-end's setting.
        Application.getInstance().getBackend().abortActiveProcess() #Abort the process going on in the back-end.
        self._current_device = ip_address
        self.deviceChanged.emit()
    
    ##  Gets the IP address of the currently selected device.
    #
    #   The IP address functions as identifier here.
    #
    #   \return The IP address of the currently selected device.
    @pyqtProperty(str,notify = deviceChanged)
    def currentDevice(self):
        return self._current_device
