import os
from PyQt5.QtQml import qmlRegisterSingletonType

from UM.Application  import Application #To get the global container stack to find the settings to send to the engine.
from UM.Backend.Backend import Backend
from UM.Preferences import Preferences
from UM.Operations.AddSceneNodeOperation import AddSceneNodeOperation
from UM.Mesh.MeshData import MeshData
import struct
from UM.Application import Application
from UM.Scene.GroupDecorator import GroupDecorator
from UM.Scene.PointCloudNode import PointCloudNode
from UM.Scene.SceneNode import SceneNode
from PyQt5.QtGui import QImage
from UM.Signal import Signal, SignalEmitter
from UM.Logger import Logger
from UM.Scene.Selection import Selection
from UM.Math.Vector import Vector
from UM.Math.Matrix import Matrix
from UM.Math.Quaternion import Quaternion
from UM.Resources import Resources
from UM.Message import Message
from UM.i18n import i18nCatalog
from UM.PluginRegistry import PluginRegistry
import numpy

from . import ProcessMeshJob
from .ScannerEngineBackendModel import ScannerEngineBackendModel
from src.Device import Device

i18n_catalog = i18nCatalog("scanner")


## Class that is responsible for listening to the backend.
class ScannerEngineBackend(Backend, SignalEmitter):
    ##  The instance if any is created.
    #
    #   Implements the singleton pattern.
    instance = None

    ##  Implements the singleton pattern for this plug-in.
    @classmethod
    def getInstance(cls):
        if not cls.instance:
            cls.instance = ScannerEngineBackend()
        return cls.instance

    def __init__(self):
        super().__init__()
        Application.getInstance().getPreferences().addPreference("backend/location", "../UltiScanTastic/Scanner/UltiScanTastic")

        self._message_handlers["argus.proto.PointCloudPointNormal"] = self._onPointCloudMessage
        self._message_handlers["argus.proto.ProgressUpdate"] = self._onProgressUpdateMessage
        self._message_handlers["argus.proto.Image"] = self._onImageMessage
        self._message_handlers["argus.proto.StatusMessage"] = self._onStatusMessage
        self._message_handlers["argus.proto.ErrorMessage"] = self._onErrorMessage
        self._message_handlers["argus.proto.Mesh"] = self._onMeshMessage
        self._message_handlers["argus.proto.Transformation"] = self._onTransformation
        self._message_handlers["argus.proto.AddDevice"] = self._onAddDeviceMessage
        self._latest_camera_image = QImage(650, 560, QImage.Format_RGB888)
        self._latest_camera_image.fill(0) # Fill image with ones (if you don't, you get trippy random colors)
        self._active_scan_node = None # Node that is used as group node for active scan is placed here.
        self.backendConnected.connect(self._onBackendConnected)
        self._backend_connected = False
        self._global_container_stack = None #The current global container stack. Not yet initialised when this class is created.
        Application.getInstance().globalContainerStackChanged.connect(self._onGlobalContainerStackChanged)

        from .ScannerEngineBackendModel import ScannerEngineBackendModel
        qmlRegisterSingletonType(ScannerEngineBackendModel, "Argus", 1, 0, "ScannerEngineBackend", ScannerEngineBackendModel.createScannerEngineBackend)

    processStarted = Signal()
    nodeAddedToActiveScan = Signal()

    def _onBackendConnected(self):
        with open(Resources.getPath(Resources.DefinitionContainers, "ultiscantastic.def.json"), "r", encoding = "utf-8") as myfile:
            data = myfile.read()
        self.sendJSON(data)
        self.initializeHardware()
        self._backend_connected = True
        Logger.log("d","Connected to backend.")

    ##  When the global container stack changes, we need to listen to the new
    #   container stack for setting changes.
    #
    #   Since we currently never change the global container stack, this only
    #   happens once at the start. That is required because the global container
    #   stack is loaded later than the plug-in is initialised.
    def _onGlobalContainerStackChanged(self):
        if self._global_container_stack:
            self._global_container_stack.propertyChanged.disconnect(self._onSettingChanged)
        self._global_container_stack = Application.getInstance().getGlobalContainerStack()
        self._global_container_stack.propertyChanged.connect(self._onSettingChanged)

    def abortActiveProcess(self):
        message = self._socket.createMessage("argus.proto.AbortProcess")
        self._socket.sendMessage(message)

    def initializeHardware(self):
        message = self._socket.createMessage("argus.proto.InitializeHardware")
        self._socket.sendMessage(message)

    def _onSettingChanged(self, setting, property):
        if property == "value":
            value = self._global_container_stack.getProperty(setting, "value")
            data_type = self._global_container_stack.getProperty(setting, "type")
            self.sendSetting(setting, value, data_type)

    def getActiveScanNode(self):
        return self._active_scan_node

    def _onStatusMessage(self, message):
        #if message.status == ultiscantastic_pb2.StatusMessage.OBJECT_NOT_FOUND:
        #    self.StatusMessage.emit("Object")
        if message.status == message.getEnumValue("PROCESSING"):
            self.StatusMessage.emit("Processing")
        if message.status == message.getEnumValue("CAPTURING"):
            self.StatusMessage.emit("Capturing")
    StatusMessage = Signal()

    def _onErrorMessage(self, message):
        error_string = ""
        if message.error == message.getEnumValue("CALIBRATION_OBJECT_NOT_FOUND"):
            self.ErrorMessage.emit("Object")
            error_string = "Calibration object not found."
        elif message.error == message.getEnumValue("PLATFORM_NOT_FOUND"):
            self.ErrorMessage.emit("Platform")
            error_string = "Platform not found. Please check if it is connected."
        elif message.error == message.getEnumValue("CAMERA_NOT_FOUND"):
            self.ErrorMessage.emit("Camera")
            error_string = "Camera not found. Please check if it is connected."
        elif message.error == message.getEnumValue("PROJECTOR_NOT_FOUND"):
            self.ErrorMessage.emit("Projector")
            error_string = "Projector not found. Please check if it is connected."
        elif message.error == message.getEnumValue("INSUFFICIENT_POINTS_HOMOGRAPHY"):
            self.ErrorMessage.emit("Homography")
            error_string = "Insufficient points found for matching. Increase window size."
        elif message.error == message.getEnumValue("UNABLE_TO_CAPTURE_FRAME"):
            self.ErrorMessage.emit("Capture")
            error_string = "Unable to capture camera frame. Check camera connection."
        elif message.error == message.getEnumValue("SETTINGS_CORRUPTED"):
            self.ErrorMessage.emit("Settings")
            error_string = "Settings corrupted, please double check validity of json file."
        elif message.error == message.getEnumValue("SCANNER_CONNECTION_FAILED"):
            self.ErrorMessage.emit("Connection")
            error_string = "Unable to connect with scanner."
        else:
            error_string = "An unknown error occured!"

        message = Message(i18n_catalog.i18n(error_string))
        message.show()
    ErrorMessage = Signal()



    ##  Handler for (rigid) transformation from backend.
    def _onTransformation(self, message):
        Logger.log("d", "Recieved transformation")
        for node in Application.getInstance().getController().getScene().getRoot().getAllChildren():
            if int(message.id) == int(id(node)): #found the node where this scan needs to be added to.
                temp_mat = Matrix()
                temp_mat._data[0, 0] = message.m00
                temp_mat._data[0, 1] = message.m01
                temp_mat._data[0, 2] = message.m02
                temp_mat._data[0, 3] = 0
                temp_mat._data[1, 0] = message.m10
                temp_mat._data[1, 1] = message.m11
                temp_mat._data[1, 2] = message.m12
                temp_mat._data[1, 3] = 0
                temp_mat._data[2, 0] = message.m20
                temp_mat._data[2, 1] = message.m21
                temp_mat._data[2, 2] = message.m22
                temp_mat._data[2, 3] = 0
                temp_mat._data[0, 3] = message.m03
                temp_mat._data[1, 3] = message.m13
                temp_mat._data[2, 3] = message.m23
                temp_mat._data[3, 3] = 1
                current_transformation = node.getLocalTransformation()
                combined = temp_mat.multiply(current_transformation)
                # Reset position &  orientation
                #node.setPosition(Vector(0,0,0))
                #node.setOrientation(Quaternion())

                temp_quaternion = Quaternion()
                temp_quaternion.setByMatrix(combined)
                node.setPosition(Vector(combined.at(0,3), combined.at(1,3), combined.at(2,3)))
                node.setOrientation(temp_quaternion)

    def getLatestCameraImage(self):
        return self._latest_camera_image

    ##  Create (libArcus) socket and registeres the message types.
    def _createSocket(self):
        super()._createSocket(os.path.abspath(os.path.join(PluginRegistry.getInstance().getPluginPath(self.getPluginId()), "Argus.proto")))

    def startScan(self, type = 0):
        self.processStarted.emit()
        message = self._socket.createMessage("argus.proto.StartScan")
        group_node = SceneNode()
        group_node.addDecorator(GroupDecorator())
        self.setActiveScanNode(group_node)
        name = "Scan"
        if type == 0:
            message.type = message.getEnumValue("GREY")
            name += " grey"
        elif type == 1:
            message.type = message.getEnumValue("PHASE")
            name += " phase"

        message.id = id(group_node)
        group_node.setName(name)
        operation = AddSceneNodeOperation(group_node,Application.getInstance().getController().getScene().getRoot())
        Application.getInstance().getOperationStack().push(operation)
        self._socket.sendMessage(message)

    ##  Convenience function
    def stitchSelectedClouds(self):
        self.stitchClouds(Selection.getAllSelectedObjects())

    ## Send a set of nodes (that contain point clouds) to the engine so they can be stitched.
    def stitchClouds(self, nodes):
        message = self._socket.createMessage("argus.proto.StitchClouds")
        for node in nodes:
            if node.getMeshData() != None and type(node) is PointCloudNode:
                cloud_message = message.addRepeatedMessage("clouds")
                cloud_message.vertices = node.getMeshDataTransformed().getVerticesAsByteArray()
                cloud_message.normals = node.getMeshDataTransformed().getNormalsAsByteArray()
                cloud_message.id = id(node)

        self._socket.sendMessage(message)

    ##  Send Setting JSON to engine (to ensure that they are identicial)
    def sendJSON(self, settingJSON):
        Logger.log('d', "Sending JSON file to engine")
        message = self._socket.createMessage("argus.proto.SettingJSON")
        message.json = settingJSON
        self._socket.sendMessage(message)

    def manualAlignClouds(self, node_1, node_2, vert_list_1, vert_list_2):
        Logger.log('d', "Manually aligning clouds")
        message = self._socket.createMessage("argus.proto.ManualAlignClouds")
        message.node_id_1 = id(node_1)
        message.node_id_2 = id(node_2)
        nodes = list(node_1.getChildren())  # Copy the list to prevent recursion
        nodes.append(node_1)
        for node in nodes:
            if node.getMeshData() is not None and type(node) is PointCloudNode:
                transformed_mesh = node.getMeshDataTransformed()
                cloud_message = message.addRepeatedMessage("clouds_1")
                cloud_message.vertices = transformed_mesh.getVerticesAsByteArray()
                cloud_message.normals = transformed_mesh.getNormalsAsByteArray()
                cloud_message.id = id(node)

        nodes = list(node_2.getChildren())  # Copy the list to prevent recursion
        nodes.append(node_2)
        for node in nodes:
            if node.getMeshData() is not None and type(node) is PointCloudNode:
                transformed_mesh = node.getMeshDataTransformed()
                cloud_message = message.addRepeatedMessage("clouds_2")
                cloud_message.vertices = transformed_mesh.getVerticesAsByteArray()
                cloud_message.normals = transformed_mesh.getNormalsAsByteArray()
                cloud_message.id = id(node)

        for vert in vert_list_1:
            vert_message = message.addRepeatedMessage("selected_points_set_1")
            vert_message.x = float(vert[0])
            vert_message.y = float(vert[1])
            vert_message.z = float(vert[2])

        for vert in vert_list_2:
            vert_message = message.addRepeatedMessage("selected_points_set_2")
            vert_message.x = float(vert[0])
            vert_message.y = float(vert[1])
            vert_message.z = float(vert[2])

        self._socket.sendMessage(message)

    ##  Send a node (that contains a cloud) to the engine so the outliers can be removed.
    def removeOutliers(self, node):
        message = self._socket.createMessage("argus.proto.StatisticalOutlierRemoval")
        message.cloud.id = id(node)
        message.cloud.vertices = node.getMeshData().getVerticesAsByteArray()
        message.cloud.normals = node.getMeshData().getNormalsAsByteArray()
        self._socket.sendMessage(message)

    ##  Send a key value pair of the setting to the engine.
    def sendSetting(self, key, value, type):
        Logger.log("d", "Sending setting to engine %s, %s", key, value)
        message = self._socket.createMessage("argus.proto.Setting")
        message.key = key
        message.value = str(value)
        if type == 'int':
            message.type = message.getEnumValue("INT")
        elif type == 'enum':
            message.type = message.getEnumValue("STRING")
        elif type == 'string':
            message.type = message.getEnumValue("STRING")
        elif type == 'float':
            message.type = message.getEnumValue("FLOAT")
        self._socket.sendMessage(message)

    def sendPointcloud(self, node):
        message = self._socket.createMessage("argus.proto.PointCloudWithNormals")
        message.vertices = node.getMeshData().getVerticesAsByteArray()
        message.normals = node.getMeshData().getNormalsAsByteArray()
        message.id = id(node)
        self._socket.sendMessage(message)

    ## Recalculate the normals of the pointcloud. TODO; viewpoint
    def recalculateNormals(self, node):
        message = self._socket.createMessage("argus.proto.RecalculateNormal")
        message.cloud.vertices = node.getMeshData().getVerticesAsByteArray()
        message.cloud.normals = node.getMeshData().getNormalsAsByteArray()
        message.cloud.id = id(node)
        message.view_point.x = 0
        message.view_point.y = 0
        message.view_point.z = 0
        message.radius = 2.5
        message.id = id(node)
        self._socket.sendMessage(message)

    ##  Create a model from the pointclouds using poisson solver.
    def poissonModelCreation(self, clouds):
        message = self._socket.createMessage("argus.proto.PoissonModelCreation")
        message.depth = 8
        message.num_samples_per_node = 1
        message.iso_divide = 8

        for cloud in clouds:
            cloud_message = message.addRepeatedMessage("clouds")
            cloud_message.vertices += cloud.getVerticesAsByteArray()
            cloud_message.normals += cloud.getNormalsAsByteArray()
            cloud_message.id = 0

        self._socket.sendMessage(message)

    def getEngineCommand(self):
        return [Application.getInstance().getPreferences().getValue("backend/location"), '--p', "{0}".format(self._port)]

    ##  Handler for recieving pointclouds
    def _onPointCloudMessage(self, message):
        #TODO: For debug purposes this is easier, but it should be moved to a job.
        Logger.log('d', "Received point cloud")
        vert_list = self._convertBytesToVerticeWithNormalsListPCL(message.data)
        if len(vert_list) == 0:
            return

        vertices = numpy.zeros((len(vert_list), 3), dtype = numpy.float32)
        normals = numpy.zeros((len(vert_list), 3), dtype = numpy.float32)
        for index, vertex in enumerate(vert_list):
            vertices[index, 0] = vertex[0]
            vertices[index, 1] = vertex[1]
            vertices[index, 2] = vertex[2]
            normals[index, 0] = vertex[3]
            normals[index, 1] = vertex[4]
            normals[index, 2] = vertex[5]
        received_mesh = MeshData(vertices, normals)

        if not message.inplace:
            Logger.log('d', "New cloud recieved %s" , message.id)
            pointcloud_node = PointCloudNode()
            pointcloud_node.setMeshData(received_mesh)
            for node in Application.getInstance().getController().getScene().getRoot().getAllChildren():
                if int(message.id) == int(id(node)): #found the node where this scan needs to be added to.
                    Logger.log("d", "Adding new cloud to group")
                    pointcloud_node.setName(node.getName() + " "+ str(len(node.getChildren())))
                    pointcloud_node.setParent(node)
                    pointcloud_node.setVisible(True)
                    return
            Logger.log("d", "Adding cloud to root")
            pointcloud_node.setParent(Application.getInstance().getController().getScene().getRoot()) #Group is deleted?
        else:
            Logger.log("d", "In place pointcloud recieved! %s" , message.id)
            for node in Application.getInstance().getController().getScene().getRoot().getAllChildren():
                if int(message.id) == int(id(node)):  # Found the node where this scan needs to be added to.
                    node.setMeshData(received_mesh)  # Overide the mesh data

    ##   Handler for mesh message, creates & starts a processmeshjob (so handing is async)
    def _onMeshMessage(self, message):
        Logger.log('d', "Recieved Mesh")
        job = ProcessMeshJob.ProcessMeshJob(message)
        job.start()

    ##  Handle image sent by engine
    def _onImageMessage(self, message):
        # Check if received image is RGB or Mono type
        if message.type == message.getEnumValue("RGB"):
            image = QImage(message.data, message.width, message.height, QImage.Format_RGB888)
        elif message.type == message.getEnumValue("MONO"):
            data = numpy.fromstring(message.data,numpy.uint8)
            resized_data = numpy.resize(data,(message.width,message.height))
            multi_channel = numpy.dstack((resized_data,resized_data,resized_data))
            image = QImage(multi_channel.tostring(),message.width,message.height,QImage.Format_RGB888)
        else:
            image = None

        self.newCameraImage.emit()
        self._latest_camera_image = image

    newCameraImage = Signal()

    ##  Set the current active node (eg; The node to which incomming scans need to be added).
    #   This node is assumed to be a GroupNode, but this is not enforced.
    def setActiveScanNode(self, node):
        if node:
            node.childrenChanged.connect(self.nodeAddedToActiveScan)
            Logger.log('d', "Active scan node set to %s", id(node))
        else:
            if self._active_scan_node:
                Logger.log('d', "Active scan node disabled")
                self._active_scan_node.childrenChanged.disconnect(self.nodeAddedToActiveScan)
        self._active_scan_node = node

    def _onProgressUpdateMessage(self, message):
        #print('amount', message.amount)
        if message.amount == 100:
            self.setActiveScanNode(None)
        self.processingProgress.emit(message.amount)

    '''def _addPointCloudWithNormals(self, data):
        app = Application.getInstance()
        recieved_mesh = MeshData()
        for vert in self._convertBytesToVerticeWithNormalsListPCL(data):
            recieved_mesh.addVertexWithNormal(vert[0],vert[1],vert[2],vert[3],vert[4],vert[5])
        node = PointCloudNode(app.getController().getScene().getRoot())
        node.setMeshData(recieved_mesh)
        operation = AddSceneNodeOperation(node,app.getController().getScene().getRoot())
        app.getOperationStack().push(operation)'''

    ## Set the step of the process (scanning, calibration, etc)
    def setProcessStep(self, step):
        Logger.log('d', "Recieved process step %s", step)
        if "Calibrate" in step:
            message = self._socket.createMessage("argus.proto.setCalibrationStep")
            calibration_step = int(step[-1:]) # Get latest char of string
            if calibration_step == 1:
                message.step = message.getEnumValue("CAMERA_FOCUS")
            elif calibration_step == 2:
                message.step = message.getEnumValue("PROJECTOR_FOCUS")
            elif calibration_step == 3:
                message.step = message.getEnumValue("CAMERA_EXPOSURE")
            elif calibration_step == 4:
                message.step = message.getEnumValue("COMPUTE")
            elif calibration_step == 5:
                message.step = message.getEnumValue("PLATFORM")
            self._socket.sendMessage(message)
        if "Scan" in step:
            message = self._socket.createMessage("argus.proto.setScanStep")
            scan_step = int(step[-1:]) ## Get latest char of string
            if scan_step == 1:
                message.step = message.getEnumValue("SETUP")
            if scan_step == 2:
                message.step = message.getEnumValue("COMPUTE")
            self._socket.sendMessage(message)

        """if step >= 3 and step <= 7:
            message = ultiscantastic_pb2.setCalibrationStep()
            if step == 4:
                message.step = ultiscantastic_pb2.setCalibrationStep.PROJECTOR_FOCUS
            elif step == 5:
                message.step = ultiscantastic_pb2.setCalibrationStep.CAMERA_FOCUS
            elif step == 6:
                message.step = ultiscantastic_pb2.setCalibrationStep.CAMERA_EXPOSURE
            elif step == 7:
                message.step = ultiscantastic_pb2.setCalibrationStep.COMPUTE
            self._socket.sendMessage(message)
        else:
            if step == 11:
                self.startScan()
            elif step == 8:
                message = ultiscantastic_pb2.setCalibrationStep()
                message.step = ultiscantastic_pb2.setCalibrationStep.PLATFORM
                self._socket.sendMessage(message)
            elif step == 13:
                self.stitchClouds(Selection.getAllSelectedObjects())
            elif step == 14:
                selected_nodes = Selection.getAllSelectedObjects()

                self.poissonModelCreation([node.getMeshData() for node in selected_nodes if node.getMeshData() is not None])


        #message = ultiscantastic_pb2.setCalibrationStep()
        #message.step = ultiscantastic_pb2.setCalibrationStep.PLATFORM"""
        pass
    
    ##  Signal to notify the backend proxy of new devices.
    addDevice = Signal()
    
    ##  Message handler to handle messages about new devices.
    #
    #   This packages the device into a device class, then signals the back-end
    #   proxy with a new device signal.
    def _onAddDeviceMessage(self,message):
        Logger.log("d","Received add-device message. Name: %s",message.name)
        device = Device(message.ipAddress,message.name,message.numCameras,message.numProjectors,message.numPlatforms)
        self.addDevice.emit(device)

    def _getTransformationFromBytes(self, data):
        pass

    ## Convert byte array using pcl::pointNormal type
    def _convertBytesToVerticeWithNormalsListPCL(self, data):
        result = []
        if len(data) > 48:
            derp = struct.unpack('ffffffffffff',data[0:48])
            if not (len(data) % 48):
                if data is not None:
                    for index in range(0,int(len(data) / 48)): #For each 48 bits (12 floats)
                        #PCL sends; x,y,z,1.0,n_x,n_y,n_z,0.0,curvatureX,curvatureY,curvatureZ,0)
                        decoded_data = struct.unpack('ffffffffffff',data[index*48:index*48+48])
                        result.append((decoded_data[0],decoded_data[1],decoded_data[2],decoded_data[4],decoded_data[5],decoded_data[6]))
                    return result
            else:
                Logger.log("e", "Data length was incorrect for requested type")
                return []
        else:
            Logger.log("d", "Recieved empty data list in conversion step.")
            return []