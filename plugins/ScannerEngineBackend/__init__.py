from plugins.ScannerEngineBackend import ScannerEngineBackend

def getMetaData():
    return {
    }

def createCameraImageProvider(engine, scriptEngine):
    return CameraImageProvider.CameraImageProvider()

def register(app):
    return {"backend": ScannerEngineBackend.ScannerEngineBackend.getInstance()}