from UM.Job import Job
from UM.Scene.SceneNode import SceneNode
from UM.Application import Application
from UM.Mesh.MeshData import MeshData
from UM.Operations.AddSceneNodeOperation import AddSceneNodeOperation
import numpy
from UM.Mesh.MeshBuilder import MeshBuilder

class ProcessMeshJob(Job):
    def __init__(self, message):
        super().__init__()
        self._message = message

    def run(self):
        vertices , indices = self._convertBytesToMesh(self._message.vertices, self._message.indices)
        mesh_builder = MeshBuilder()
        mesh_builder.setIndices(indices)
        mesh_builder.setVertices(vertices)

        mesh_builder.calculateNormals()
        mesh_data = mesh_builder.build()

        node = SceneNode()
        node.setMeshData(mesh_data)
        node.setSelectable(True)
        operation = AddSceneNodeOperation(node, Application.getInstance().getController().getScene().getRoot())
        operation.push()

    def _convertBytesToMesh(self, verts_data, indices_data):
        verts = numpy.fromstring(verts_data,dtype=numpy.float32)
        verts = verts.reshape(-1, 4) # Reshape list to pairs of 4 (as they are sent as homogenous data)
        verts =  verts[:, 0:3] # Cut off the homogenous coord.
        indices = numpy.fromstring(indices_data,dtype=numpy.uint32)
        indices = indices.reshape(-1, 3)
        return (verts,indices)   
