#!/dev/python

from PyQt5.QtCore import pyqtSignal, pyqtProperty, pyqtSlot, QObject
from PyQt5.QtQml import qmlRegisterSingletonType
from PyQt5.QtCore import QUrl
from UM.Scene.Selection import Selection


##  Model that exposes properties of the scanner engine back-end to QML.
class ScannerEngineBackendModel(QObject):
    ##  Creates a back-end model for QML to abuse.
    @staticmethod
    def createScannerEngineBackend(engine, script_engine):
        return ScannerEngineBackendModel()

    ##  Creates the back-end model.
    #
    #   This should only be called via the qmlRegisterSingletonType.
    def __init__(self):
        super().__init__()
        from .ScannerEngineBackend import ScannerEngineBackend
        self._backend = ScannerEngineBackend.getInstance()
        self.cameraImageChanged.connect(self._backend.newCameraImage)
        self._camera_image_id = 0

    ##  Signifies that the camera image was updated.
    cameraImageChanged = pyqtSignal()

    ##  Gets the latest camera image.
    @pyqtProperty("QVariant", notify = cameraImageChanged)
    def cameraImage(self):
        self._camera_image_id += 1
        temp = "image://camera/" + str(self._camera_image_id)
        return QUrl(temp, QUrl.TolerantMode)

    ##  Starts a scan (asynchronously, it doesn't wait for the scan to complete).
    @pyqtSlot()
    def scan(self):
        self._backend.startScan()

    @pyqtSlot()
    def stitchSelectedClouds(self):
        self._backend.stitchSelectedClouds()

    @pyqtSlot()
    def createMeshFromSelected(self):
        selected_nodes = Selection.getAllSelectedObjects()
        self._backend.poissonModelCreation([node.getMeshData() for node in selected_nodes if node.getMeshData() is not None])
