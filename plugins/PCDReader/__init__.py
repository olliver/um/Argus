from . import PCDReader


def getMetaData():
    return {
        "mesh_reader":
        [{
            "extension": "pcd",
            "description": "pcdFile"
        }]
    }


def register(app):
    return {"mesh_reader":PCDReader.PCDReader()}
