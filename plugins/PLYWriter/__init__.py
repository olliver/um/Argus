from . import PLYWriter


#TODO: We can't quite finish this as we have no real faces to save yet. This writer should work, but is not tested.
def getMetaData():
    return {
        "mesh_writer":{
            "output": [
                {
                    "extension": "ply",
                    "description": "PLY File"
                }
            ]
        }
    }


def register(app):
    return {"mesh_writer": PLYWriter.PLYWriter()}
