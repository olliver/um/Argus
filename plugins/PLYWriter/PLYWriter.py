from UM.Mesh.MeshWriter import MeshWriter
from UM.Scene.Platform import Platform
import time


class PLYWriter(MeshWriter):
    def write(self, stream, nodes, mode = MeshWriter.OutputMode.TextMode):
        num_verts = 0

        nodes_to_save = []

        for node in nodes:
            if type(node) == Platform:
                continue
            if node.getMeshData():
                nodes_to_save.append(node)
                num_verts += node.getMeshData().getVertexCount() - 1

            children_nodes = node.getAllChildren()
            for child_node in children_nodes:
                if type(child_node) == Platform:
                    continue
                if child_node.getMeshData():
                    nodes_to_save.append(child_node)
                    num_verts += child_node.getMeshData().getVertexCount() - 1


        stream.write("ply \n")  # Magic number that the file needs to start with
        stream.write("format ascii 1.0\n")  # Can also be: format binary_little_endian 1.0 or format binary_big_endian 1.0 (for binary)
        stream.write("comment ARGUS ply writer created at " + time.strftime('%a %d %b %Y %H:%M:%S') + "\n")

        stream.write("element vertex " + str(num_verts) + "\n")
        stream.write("property float x\n")  # Tell the ply that there is a property called x for vertex.
        stream.write("property float y\n")  # Tell the ply that there is a property called y for vertex.
        stream.write("property float z\n")  # Tell the ply that there is a property called z for vertex.
        stream.write("property float nx\n")  # Tell the ply that there is a property called n_x for vertex (normal).
        stream.write("property float ny\n")  # Tell the ply that there is a property called n_x for vertex (normal).
        stream.write("property float nz\n")  # Tell the ply that there is a property called n_x for vertex (normal).
        stream.write("end_header\n")  # Notify end of header  (and start of verts)

        for node in nodes_to_save:
            mesh_data = node.getMeshData()
            positions = mesh_data.getVertices()
            normals = mesh_data.getNormals()
            for index in range(0, mesh_data.getVertexCount() - 1):
                data = "" + str(positions[index][0]) + " " + str(positions[index][1]) + " " + str(positions[index][2])
                data += " " + str(normals[index][0]) + " " + str(normals[index][1]) + " " + str(normals[index][2])
                data += "\n"
                stream.write(data)

        return True
