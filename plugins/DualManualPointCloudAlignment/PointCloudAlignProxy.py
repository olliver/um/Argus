from PyQt5.QtCore import QObject, Qt
from PyQt5.QtCore import pyqtSignal, pyqtProperty, pyqtSlot
from UM.Application import Application
from UM.Qt.ListModel import ListModel
from UM.Scene.PointCloudNode import PointCloudNode

class PointCloudAlignProxy(QObject):
    NameRole = Qt.UserRole + 1
    def __init__(self, parent = None):
        super().__init__(parent)
        self._cloud_list_model = None
        self._selectable_nodes = []
        Application.getInstance().getController().getScene().sceneChanged.connect(self._onSceneChanged)
        self._selecting_points = False

    cloudListChanged = pyqtSignal()
    selectingPointsChanged = pyqtSignal()

    def _onSceneChanged(self, source):
        self.cloudListChanged.emit()

    @pyqtSlot()
    def stitchClouds(self):
        Application.getInstance().getController().getActiveTool().stitchClouds()
        self._selecting_points = False
        self.selectingPointsChanged.emit()

    @pyqtProperty(bool, notify = selectingPointsChanged)
    def selectingPoints(self):
        return self._selecting_points

    @pyqtSlot(int,int)
    def startAlignProcess(self, index1, index2):
        Application.getInstance().getController().setActiveView("ManualPointCloudAlignment")
        Application.getInstance().getController().getActiveTool().setAlignmentNodes(self._selectable_nodes[index1],self._selectable_nodes[index2])
        self._selecting_points = True
        self.selectingPointsChanged.emit()

    @pyqtProperty(QObject, notify = cloudListChanged)
    def cloudList(self):
        if not self._cloud_list_model:
            self._cloud_list_model = ListModel()
            self._cloud_list_model.addRoleName(self.NameRole, "text")
        self._cloud_list_model.clear()
        for group_node in Application.getInstance().getController().getScene().getRoot().getChildren():
            if type(group_node) is PointCloudNode or group_node.callDecoration("isGroup"):
                self._cloud_list_model.appendItem({"text":group_node.getName()})
                self._selectable_nodes.append(group_node)
        return self._cloud_list_model