import QtQuick 2.2
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.1

import "../../resources/qml"
import Argus 1.0 as Argus
import UM 1.0 as UM

Item
{
    width: childrenRect.width
    height: childrenRect.height


    Button
    {
        text: "Stitch clouds"
        id: stitch_button
        enabled: UM.ActiveTool.properties.getValue("StitchEnabled")
        visible: Argus.PointCloudAlignProxy.selectingPoints

        onClicked: Argus.PointCloudAlignProxy.stitchClouds()
    }

    Column
    {
        anchors.leftMargin: UM.Theme.getSize("default_margin").width;
        anchors.left: parent.left
        visible: !Argus.PointCloudAlignProxy.selectingPoints
        width: visible ? childrenRect.width : 0
        height: visible? childrenRect.height: 0
        spacing: UM.Theme.getSize("default_margin").width;

        Text
        {
            text: "Align cloud: "
        }

        Row
        {
            spacing: UM.Theme.getSize("default_margin").width;
            ComboBox
            {
                id: selection1
                model: Argus.PointCloudAlignProxy.cloudList
                currentIndex: 0
            }
            Text
            {
                text: "With: "
            }
            ComboBox
            {
                id: selection2
                model: Argus.PointCloudAlignProxy.cloudList
                currentIndex: 1
            }

            Button
            {
                text: "start manual align"
                enabled: selection1.currentIndex == selection2.currentIndex ? false:true
                onClicked:
                {
                    Argus.PointCloudAlignProxy.startAlignProcess(selection1.currentIndex, selection2.currentIndex)
                }
            }
        }

        SingleCategorySettingPanel
        {
            setting_category: "stitching"
            width: 470
            height: 100
            setting_width: 450
        }
    }
}