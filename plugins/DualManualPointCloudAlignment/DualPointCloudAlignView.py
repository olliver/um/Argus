import os

from UM.Application import Application
from UM.Event import Event
from UM.Math.Color import Color
from UM.Math.Matrix import Matrix
from UM.Math.Vector import Vector
from UM.PluginRegistry import PluginRegistry
from UM.Scene.Camera import Camera
from UM.Scene.Iterator.DepthFirstIterator import DepthFirstIterator
from UM.Scene.ToolHandle import ToolHandle
from UM.View.GL.OpenGL import OpenGL
from UM.View.View import View
from plugins.DualManualPointCloudAlignment.DualRenderPass import DualRenderPass
from src.VertexSelectionPass import VertexSelectionPass


class DualPointCloudAlignView(View):
    def __init__(self):
        super().__init__()

        self._composite_pass = None
        self._old_layer_bindings = []
        self._old_composite_shader = None
        self._dual_view_composite_shader = None
        self._stored_left_origin = None
        self._stored_right_origin = None

    def beginRendering(self):
        scene = self.getController().getScene()
        renderer = self.getRenderer()

        for node in DepthFirstIterator(scene.getRoot()):
            if isinstance(node, ToolHandle):
                node.render(renderer)

    def endRendering(self):
        pass

    def createCamera(self, name: str) -> Camera:
        root = Application.getInstance().getController().getScene().getRoot()
        camera = Camera(name, root)
        camera.translate(Vector(0, 150, -500))
        proj = Matrix()
        proj.setPerspective(45, 640 / 480, 1, 500)
        camera.setProjectionMatrix(proj)
        camera.setPerspective(True)
        camera.lookAt(Vector(0, 0, 0))
        return camera

    def event(self, event):
        if event.type == Event.ViewActivateEvent:
            if not self._composite_pass:
                self._composite_pass = self.getRenderer().getRenderPass("composite")

            self.getRenderer().addRenderPass(VertexSelectionPass(self.getRenderer().getViewportWidth(), self.getRenderer().getViewportHeight()))

            left_view = DualRenderPass("left_view", 1, 1)
            right_view = DualRenderPass("right_view", 1, 1)

            left_camera = self.createCamera("left_camera")
            right_camera = self.createCamera("right_camera")

            left_view.setCamera(left_camera)
            right_view.setCamera(right_camera)

            self.getRenderer().addRenderPass(left_view)
            self.getRenderer().addRenderPass(right_view)

            self._old_layer_bindings = self._composite_pass.getLayerBindings()
            self._composite_pass.setLayerBindings(["right_view", "left_view"])
            self._old_composite_shader = self._composite_pass.getCompositeShader()

            if not self._dual_view_composite_shader:
                self._dual_view_composite_shader = OpenGL.getInstance().createShaderProgram(os.path.join(PluginRegistry.getInstance().getPluginPath("DualManualPointCloudAlignment"), "dual_composite.shader"))

                theme = Application.getInstance().getTheme()
                self._dual_view_composite_shader.setUniformValue("u_background_color", Color(*theme.getColor("viewport_background").getRgb()))

            self._composite_pass.setCompositeShader(self._dual_view_composite_shader)

        elif event.type == Event.ViewDeactivateEvent:
            self._composite_pass.setLayerBindings(self._old_layer_bindings)
            self.getRenderer().removeRenderPass(self.getRenderer().getRenderPass("left_view"))
            self.getRenderer().removeRenderPass(self.getRenderer().getRenderPass("right_view"))
            self.getRenderer().removeRenderPass(self.getRenderer().getRenderPass("vertex_selection"))
            self._composite_pass.setCompositeShader(self._old_composite_shader)
            self.getController().getScene().setActiveCamera('3d')

        if event.type == Event.MouseMoveEvent:
            px = (0.5 + event.x / 2.0) * Application.getInstance().getMainWindow().width()
            if px > 0.5 * self.getRenderer().getViewportWidth():
                camera_changed = False
                if self.getController().getScene().getActiveCamera().getName() != "right_camera":
                    camera_changed = True
                    self._stored_left_origin = self.getController().getCameraTool().getOrigin()

                self.getController().getScene().setActiveCamera("right_camera")

                if self._stored_right_origin and camera_changed:
                    self.getController().getCameraTool()._origin = self._stored_right_origin

                scene = self.getController().getScene()
                for node in DepthFirstIterator(scene.getRoot()):
                    try:
                        # This in effect only change nodes that have the render_view property.
                        node.setEnabled(node.render_view == "right_view")
                    except:
                        pass
            else:
                camera_changed = False
                if self.getController().getScene().getActiveCamera().getName() != "left_camera":
                    camera_changed = True
                    self._stored_right_origin = self.getController().getCameraTool().getOrigin()

                self.getController().getScene().setActiveCamera("left_camera")

                if self._stored_left_origin and camera_changed:
                    self.getController().getCameraTool()._origin = self._stored_left_origin

                scene = self.getController().getScene()
                for node in DepthFirstIterator(scene.getRoot()):
                    try:
                        # This in effect only change nodes that have the render_view property.
                        node.setEnabled(node.render_view == "left_view")
                    except:
                        pass

        if self._dual_view_composite_shader:
            self._dual_view_composite_shader.setUniformValue("u_window_size", [self.getRenderer().getViewportWidth(), self.getRenderer().getViewportHeight()])