import QtQuick 2.2
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.1

import "../../resources/qml"
import Argus 1.0 as Argus
import UM 1.0 as UM

Item
{
    id:base
    height: max_height
    width: max_width
    property var height_toolbar: toolbar_height
    property var leftPointList: UM.ActiveTool.properties.getValue("LeftPointList")
    property var rightPointList: UM.ActiveTool.properties.getValue("RightPointList")

    property var marker_size: 5

    Repeater
    {
        model: leftPointList
        Item
        {
            // No idea where the +28 comes from, but at this point I cant be bothered to figure it out.
            // Just treat it as a magic number. Same goes for the magical +23
            // Only show the points that are to the left of the screen
            visible: leftPointList[index].x < 0
            y: (0.5 * base.height - base.height_toolbar + 23 - 0.5 * height) - leftPointList[index].y * (0.5 *  (base.height +  base.height_toolbar + 28))
            x: (0.5 * base.width - 0.5 * width) + leftPointList[index].x * (0.5 *  base.width)
            Rectangle
            {
                id: yellow_marker
                color:"yellow"
                property var test_width: base.width
                width: marker_size
                height: marker_size
            }
            Label
            {
                text: index
                color:"yellow"
                anchors.left: yellow_marker.right
                anchors.top: yellow_marker.bottom
            }
        }
    }

    Repeater
    {
        model: rightPointList
        Item
        {
            x: (0.5 * base.width - 0.5 * width) + rightPointList[index].x * (0.5 *  base.width)
            // No idea where the +28 comes from, but at this point I cant be bothered to figure it out.
            // Just treat it as a magic number. Same goes for the magical +23
            y: (0.5 * base.height - base.height_toolbar + 23 - 0.5 * height) - rightPointList[index].y * (0.5 *  (base.height +  base.height_toolbar + 28))
            // Only show the points that are to the left of the screen
            visible: rightPointList[index].x > 0

            Rectangle
            {
                id: red_marker
                color:"red"
                property var test_width: base.width

                width: marker_size
                height: marker_size
            }
            Label
            {
                text: index
                color:"red"
                anchors.left: red_marker.right
                anchors.top: red_marker.bottom
            }
        }
    }
}