from PyQt5.QtCore import QObject

from UM.Application import Application
from UM.Event import Event, MouseEvent
from UM.Math.Vector import Vector
from UM.Scene.SceneNode import SceneNode
from UM.Tool import Tool


class DualPointCloudAlignTool(QObject, Tool):
    def __init__(self, parent = None):
        super().__init__(parent)
        self._previous_view = None
        self.reset()
        self._renderer = Application.getInstance().getRenderer()

        self.setExposedProperties("LeftPointList", "RightPointList", "StitchEnabled")

    def getLeftPointList(self):
        return self._projected_left_points

    def getRightPointList(self):
        return self._projected_right_points

    def stitchClouds(self):
        # Notify backend that clouds need to be sent & aligned with backend
        Application.getInstance().getBackend().manualAlignClouds(self._left_node, self._right_node, self._left_node_point_list,
                                                                 self._right_node_point_list)

        # Reset all the tool stuff
        self._left_node.setEnabled(True)
        self._right_node.setEnabled(True)
        delattr(self._left_node, "render_view")
        delattr(self._right_node, "render_view")
        Application.getInstance().getController().setActiveTool(None)
        self.reset()

    def reset(self):

        self._left_node = None
        self._right_node = None
        self._left_node_point_list = []
        self._right_node_point_list = []

        self._projected_right_points = []
        self._projected_left_points = []

    def setAlignmentNodes(self, left_node: SceneNode, right_node: SceneNode):
        Application.getInstance().getController(). setActiveView("DualManualPointCloudAlignment")
        self._left_node = left_node
        self._right_node = right_node
        self._left_node.render_view = "left_view"
        self._right_node.render_view = "right_view"

    def getStitchEnabled(self):
        if len(self._left_node_point_list) > 2 and len(self._right_node_point_list) == len(self._left_node_point_list):
            return True
        return False

    def recalculateProjectionPoints(self):
        camera = Application.getInstance().getController().getScene().getActiveCamera()
        if camera.getName() == "left_camera":
            self._projected_left_points = []
            for point in self._left_node_point_list:
                projected_position = camera.project(Vector(point[0], point[1], point[2]))
                self._projected_left_points.append({"x": float(projected_position[0]), "y": float(projected_position[1])})
        else:
            self._projected_right_points = []
            for point in self._right_node_point_list:
                projected_position = camera.project(Vector(point[0], point[1], point[2]))
                self._projected_right_points.append({"x": float(projected_position[0]), "y": float(projected_position[1])})

        self.propertyChanged.emit()

    def event(self, event):
        # Check for tool activation & deactivation events
        if event.type == Event.ToolActivateEvent:
            Application.getInstance().getMainWindow().renderCompleted.connect(self.recalculateProjectionPoints)
            self._previous_view = Application.getInstance().getController().getActiveView().getPluginId()

        elif event.type == Event.ToolDeactivateEvent:
            Application.getInstance().getMainWindow().renderCompleted.disconnect(self.recalculateProjectionPoints)
            Application.getInstance().getController().setActiveView(self._previous_view)

        if event.type == MouseEvent.MouseReleaseEvent and MouseEvent.LeftButton in event.buttons:
            pixel_color = self._renderer.getRenderPass("vertex_selection").getColorAtMousePosition(event.x,event.y)
            if pixel_color:
                if pixel_color.a:
                    index = int(255 - pixel_color.a * 255)
                    targeted_node = Application.getInstance().getCloudNodeByIndex(index)

                    pixel_index = int(pixel_color.r * 255) + (int(pixel_color.g * 255) << 8) + (int(pixel_color.b * 255) << 16)
                    selected_vertex = targeted_node.getMeshDataTransformed().getVertex(pixel_index)
                    if targeted_node.render_view == "left_view":
                        self._left_node_point_list.append(selected_vertex)
                        self.recalculateProjectionPoints()
                    elif targeted_node.render_view == "right_view":
                        self._right_node_point_list.append(selected_vertex)
                        self.recalculateProjectionPoints()

                    self.propertyChanged.emit()
                    Application.getInstance().getController().getScene().sceneChanged.emit(self)