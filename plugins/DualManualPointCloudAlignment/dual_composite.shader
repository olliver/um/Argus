[shaders]
vertex =
    uniform highp mat4 u_modelViewProjectionMatrix;
    attribute highp vec4 a_vertex;
    attribute highp vec2 a_uvs;

    varying highp vec2 v_uvs;

    void main()
    {
        gl_Position = u_modelViewProjectionMatrix * a_vertex;
        v_uvs = a_uvs;
    }

fragment =
    uniform sampler2D u_layer0;
    uniform sampler2D u_layer1;
    uniform sampler2D u_layer2;
    uniform vec2 u_window_size;
    uniform float u_border_ratio;
    varying vec2 v_uvs;

    void main()
    {
        vec4 result = vec4(0.965, 0.965, 0.965, 1.0);
        vec4 layer0 = texture2D(u_layer0, v_uvs);
        vec4 layer1 = texture2D(u_layer1, v_uvs);

        if(gl_FragCoord.x / u_window_size[0] > u_border_ratio)
        {
            result = layer0 * layer0.a + result * (1.0 - layer0.a);
        } else
        {
            result = layer1 * 0.5;
        }
        gl_FragColor = result;
    }

[defaults]
u_layer0 = 0
u_layer1 = 1
u_border_ratio = 0.5

[bindings]

[attributes]
a_vertex = vertex
a_uvs = uv

