from . import AutomaticPointcloudAlignmentTool


def getMetaData():
    return {
        "tool": 
        {
            "name": "Automatic Cloud Alignment",
            "tool_panel": "AutomaticPointcloudAlignmentTool.qml",
            "description": "Align pointclouds",
            "icon": "",
            "visible": False
        }
    }


def register(app):
    plugin_dict = {}
    plugin_dict["tool"] = AutomaticPointcloudAlignmentTool.AutomaticPointCloudAlignmentTool()
    return plugin_dict