import QtQuick 2.2
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.1
import UM 1.0 as UM
import Argus 1.0 as Argus
import "../../resources/qml"
Item 
{
    height: 150
    width: 470
    
    Column
    {
        width: 479
        anchors.horizontalCenter: parent.horizontalCenter
        spacing: 2
        Text
        {
            text: "Select the clouds that you want to stitch."
        }
        SingleCategorySettingPanel
        {
            id: settingPanel
            setting_category: "stitching"
            width: 470
            height: 100
            setting_width: 450
        }

        WizardControlButton 
        {
            text: "Stitch"
            anchors.right: settingPanel.right
            width: 110
            onClicked: 
            {
                Argus.ScannerEngineBackend.stitchSelectedClouds()
                UM.Controller.setActiveTool(null)
            }
        }
    }
}